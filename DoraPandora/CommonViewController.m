//
//  CommonViewController.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 03.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "CommonViewController.h"
#import "ColorsManager.h"

@interface CommonViewController ()  {
    
}

@end

@implementation CommonViewController

- (void) createnNavigationBarWithoutBorders {
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
}

-(void) makeNavigationBarHidden {
    
    [self createnNavigationBarWithoutBorders];
    [self.navigationController.navigationBar setHidden:YES];
    self.navigationController.navigationBar.tintColor = [ColorsManager navigationBarIconsColor];
    
}

-(void)createNavigationBar{
    [self createnNavigationBarWithoutBorders];
    
    UIBarButtonItem *search = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"search"] style:UIBarButtonItemStyleDone target:self action:@selector(searchItem:)];
    self.navigationItem.rightBarButtonItem = search;
    
    UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu-50"] style:UIBarButtonItemStyleDone target:self action:@selector(backButton:)];
    
    // self.navigationItem.titleView.center =CGPointMake(0, 0);
    
    self.navigationItem.rightBarButtonItem = search;
    self.navigationItem.leftBarButtonItem = back;
    
}

- (IBAction)backButton:(UIButton *)sender {
    
}

- (IBAction)searchItem:(UIButton *)sender {
    
}

@end
