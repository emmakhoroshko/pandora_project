//
//  AppDelegate.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 12.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Crashlytics/Crashlytics.h>
#import <Fabric/Fabric.h>
#import <CoreData/CoreData.h>
#import "NoteManager.h"
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NotesModel *noteModel;
@property (nonatomic, strong) NoteManager  *noteManager;
@property (nonatomic, strong) ViewController  *controller;
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSPersistentStoreCoordinator *coordinator;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;
@end

