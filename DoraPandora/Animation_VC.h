//
//  Animation_VC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 05.08.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Animation_VC : NSObject
@property CGPoint titleStartedPoint;
@property (nonatomic) BOOL presenting ;

-(void)animateAppearance: (UIView *)view title:(UILabel *)titleMain icon:(UIImageView *)icon;

@end
