//
//  AlbumModel.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 21.06.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "AlbumModel.h"

@implementation AlbumModel



-(id)initAlbumCover:(UIImage *)albumCover
          albumDate:(NSDate *)albumDefaultDate
              aName:(NSString *)aName{
    self = [super init];
    if (self) {
        _albumCover = albumCover;
        _albumDate = albumDefaultDate;
        _aName = aName;
    }
    return self;
    
}

-(id)init:(NSData *)imageData
defaultDate:(NSDate *)defaultDate
albumName:(NSString *)albumName
image_path:(NSString *) imagePath
 img_name:(NSString *) imageName{
    self = [super init];
    if (self) {
        _image = imageData;
        _imageDate = defaultDate;
        _albumName =albumName;
        _imagePath =imagePath;
        _imageName =imageName;
    }
    return self;
    
}

@end
