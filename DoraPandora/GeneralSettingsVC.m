//
//  GeneralSettingsVC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 14.09.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "GeneralSettingsVC.h"
#import "SettingsTableViewCell.h"

@interface GeneralSettingsVC () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation GeneralSettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavigationBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
-(void)createNavigationBar{
    UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu-50"] style:UIBarButtonItemStyleDone target:self action:@selector(backButton:)];
    
    // self.navigationItem.titleView.center =CGPointMake(0, 0);
    self.navigationItem.leftBarButtonItem = back;
    
}
- (IBAction)backButton:(UIButton *)sender{
    
    [self.navigationController setNavigationBarHidden:true];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SettingsTableViewCell *cell = [_tableViewSettings dequeueReusableCellWithIdentifier:@"settingCell"];
    
    
    return cell;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
