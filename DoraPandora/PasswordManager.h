//
//  PasswordManager.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 22.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NotesModel.h"
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SearchScreen_Cell_VC.h"


@interface PasswordManager : NSObject

@property (nonatomic) int searchArrayCount;

-(NSArray *)passwords;

-(void)addPasswordInfo:(NotesModel *)pass;

-(void)deletePasswordatIndex:(NotesModel *)pass;
-(SearchScreen_Cell_VC *)searchPasswordsContainString:(NSString*)string indexPath:(NSIndexPath *)index tableview: (UITableView*)tableView;
-(NSArray *)searchPasswordsContainString:(NSString*)string;

-(NSString *)dateFormatted ;
-(BOOL)isObjectExistAtIndex:(NSInteger)index;
-(NSArray *)sortedPasswords;
@end
