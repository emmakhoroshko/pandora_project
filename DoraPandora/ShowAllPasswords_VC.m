//
//  ShowAllPasswords_VC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 22.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "ShowAllPasswords_VC.h"
#import "ViewDesign.h"
#import "PasswordTableViewCell.h"
#import "ColorsManager.h"
#import "NotesModel.h"
#import "OnePassword_VC.h"
#import "Animation_VC.h"
#define duration 0.5

@interface ShowAllPasswords_VC () <UITableViewDataSource,UITableViewDelegate>{
    
    NotesModel *newPass;
    double screenCenter;
    Animation_VC *animation;
    
}

@end


#pragma - mark - Life cycle

@implementation ShowAllPasswords_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _passwordsTableView.delegate =self;
    _passwordsTableView.dataSource =self;
    
    _passwords =[_pwManager sortedPasswords];
    [ViewDesign setRoundView:_designView andImage:_mainPasswordIcon];
    _mainPasswordIcon.image = [UIImage imageNamed:@"password_icon"];
   
    [self createnNavigationBarWithoutBorders];

    animation=[[Animation_VC alloc]init];
    _titleMain = [[UILabel alloc]init];
    [self initLabel];
    
}
-(void)viewWillAppear:(BOOL)animated {
    
    
     _passwords =[_pwManager sortedPasswords];
    [_passwordsTableView reloadData];
    [self animateAppearance];
    [self createNavigationBar];
    if (_animateTransition) {
         [self.navigationController setNavigationBarHidden:true];
    }
    
    [self createToolBar];
    
  //  
    [super viewWillAppear:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PasswordTableViewCell *cell = [_passwordsTableView dequeueReusableCellWithIdentifier:@"PasswordTableViewCell"];
    NotesModel *pass = _passwords[indexPath.item];
    cell.passwordTitle.text = pass.pwTitle;
    cell.passwordLogin.text =pass.pwLogin;
    cell.pwPassword.text =pass.pwPassword;
    cell.pwAdditionalInfo.text= pass.pwAdditionalInfo;
    return cell;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   return [_passwords count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [_passwordsTableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"OnePassword_VC" sender:indexPath];
    
}

#pragma - mark - ToolBar
- (void)createToolBar{
    UIBarButtonItem *newItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"newItem"] style:UIBarButtonItemStyleDone target:self action:@selector(createNewPassword:)];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    NSArray *toolBarItems = [[NSArray alloc]initWithObjects:spacer,newItem,spacer, nil];
    [_addItemToolBar setItems:toolBarItems animated:YES];
    
}
-(IBAction)createNewPassword:(id)sender {
    [self performSegueWithIdentifier:@"OnePassword_VC" sender:nil];
    
}

#pragma - mark -Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    _animateTransition = false;
    
    if ([segue.identifier isEqualToString:@"OnePassword_VC"]) {
        OnePassword_VC *vc = [segue destinationViewController];
        vc.manager =_pwManager;
        
        if (sender){
            
            NSIndexPath *indexPath = (NSIndexPath *)sender;
            vc.password = _passwords[indexPath.item];
            vc.currentIndex = (int)[[_pwManager passwords]indexOfObject:_passwords[indexPath.item]];
            NSLog(@"%d vc.currentIndex2",vc.currentIndex);
            vc.newPasswordExample = NO;
        }else{
            [self newPassword];
            vc.password =newPass;
            vc.currentIndex =(int)[[_pwManager passwords]indexOfObject:newPass];
            NSLog(@"%d vc.currentIndex3",vc.currentIndex);
            vc.newPasswordExample = YES;
        }
        
    }
}


-(void)newPassword {
    NSString *date =[_pwManager dateFormatted];
    NSDate *now =[NSDate date];
    newPass =[[NotesModel alloc]initPasswordWithTitle:NSLocalizedString(@"new_password", nil)login:@"" password:@"" additionalInfo:@"" comment:date date: now ];
   // [_pwManager addPasswordInfo:newPass];
    
}



#pragma - mark -Animation

-(void)animateAppearance{
    
    if (_animateTransition) {
        animation.presenting =_presenting;
        
        if (_presenting) {
            
            _passwordsTableView.hidden = false;
            _designView.hidden = false;
            _passwordsTableView.layer.opacity =0;
            _designView.layer.opacity =0;
            
        }
        
        [animation animateAppearance:self.view title:_titleMain icon:_mainPasswordIcon];
        
        [UIView animateWithDuration:duration delay:0.0 options:0.0 animations:^{
            
            _passwordsTableView.layer.opacity =_presenting ? 1:0;
            _designView.layer.opacity =_presenting ? 1:0;
            
            
        } completion:^(BOOL finished) {
            if (_presenting) {
                [self.navigationController setNavigationBarHidden:false];
            }
        }];
        
        
    }
    
}



#pragma - mark - NavigationBar


- (IBAction)searchItem:(UIButton *)sender{
    
}

- (IBAction)backButton:(UIButton *)sender{

    _animateTransition = true;
    _presenting = false;
    [self.navigationController setNavigationBarHidden:true];
    [self animateAppearance];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];

}


-(void)initLabel{
    
    _titleMain=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150 , 53)];
    _titleMain.textColor = [UIColor whiteColor];
    _titleMain.userInteractionEnabled=NO;
    _titleMain.text= NSLocalizedString(@"passwords", nil);
    _titleMain.font =[UIFont systemFontOfSize:18];
    _titleMain.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_titleMain];
    //_labelStartedPoint =CGPointMake(114, 49);
}
@end
