//
//  Animation_VC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 05.08.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "Animation_VC.h"
#define duration 0.5
#define animDelay 0


@implementation Animation_VC



-(void)animateAppearance: (UIView *)view title:(UILabel *)titleMain icon:(UIImageView *)icon{
    _titleStartedPoint = CGPointMake(114, 49);
        
        double yScaleFactor =0.5;
        
        //
        CGAffineTransform scaleTransform = CGAffineTransformScale(CGAffineTransformIdentity , yScaleFactor, yScaleFactor+0.1);
        
        CGAffineTransform scaleTransformTitle = CGAffineTransformScale(CGAffineTransformIdentity , 1.1, 1.35);
        
        float screenCenter =  view.center.x;
        
        
        CGPoint initialCenterPointIcon = _presenting? CGPointMake(45, 12) : CGPointMake(screenCenter, 76);
        
        
        CGPoint finalCenterPointIcon = _presenting? CGPointMake(screenCenter, 76):CGPointMake(45, 48);
        
        
        NSLog(@"finalCenterPointIcon %f",finalCenterPointIcon.x);
        
        CGPoint initialCenterTitle = (_presenting)?  _titleStartedPoint : CGPointMake(screenCenter, 40);
        
        CGPoint finalCenterTitle = _presenting ? CGPointMake(screenCenter, 40) : _titleStartedPoint;
        
        
        
        
    
        
        NSLog(@"screenCenter1 %f",screenCenter);
        //  self.navigationItem.titleView = title;
        //   self.navigationItem.titleView.frame = initialFrameTitle;
        
        //   self.navigationItem.titleView = title;
        
        
        if (_presenting) {
            
            
//            _notesTableView.hidden = false;
//            _designRoundView.hidden = false;
//            _notesTableView.layer.opacity =0;
//            _designRoundView.layer.opacity =0;
            
            icon.transform = scaleTransform;
            icon.center = initialCenterPointIcon;
            
            titleMain.layer.position = initialCenterTitle;
            titleMain.transform = scaleTransformTitle;
            
            
        }
        
        [UIView animateWithDuration:duration delay:animDelay options:0.0 animations:^{
            //
            
            titleMain.center = finalCenterTitle;
            
            titleMain.transform = (_presenting)? CGAffineTransformIdentity : scaleTransformTitle;
            
            icon.transform = (_presenting)? CGAffineTransformIdentity : scaleTransform;
            icon.center = finalCenterPointIcon;
            
                  } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.0
                             animations:^{
                             }completion:^(BOOL finished) {
                                 
                             }];
        }];
    }
    
    
    





@end
