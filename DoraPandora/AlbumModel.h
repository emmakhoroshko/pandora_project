//
//  AlbumModel.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 21.06.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface AlbumModel : NSObject
@property (strong, nonatomic) UIImage * albumCover;
@property (strong, nonatomic) NSDate * albumDate;
@property (strong, nonatomic) NSString * aName;

-(id)initAlbumCover:(UIImage *)albumCover
          albumDate:(NSDate *)albumDefaultDate
              aName:(NSString *)aName;

@property (strong, nonatomic) NSData * image;
@property (strong, nonatomic) NSDate * imageDate;
@property (strong, nonatomic) NSString * albumName;
@property (strong, nonatomic) NSString * imagePath;
@property (strong, nonatomic) NSString * imageName;

-(id)init:(NSData *)imageData
defaultDate:(NSDate *)defaultDate
  albumName:(NSString *)albumName
 image_path:(NSString *) imagePath
   img_name:(NSString *) imageName;
@end
