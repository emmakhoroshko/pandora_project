//
//  ShowAllAlbums_VC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 16.06.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaManager.h"
#import "PhotoManager.h"

@interface ShowAllAlbums_VC : UIViewController
@property (strong, nonatomic) IBOutlet UIView *designRoundView;
@property (strong, nonatomic) IBOutlet UICollectionView *photoCollectionView;
@property (strong, nonatomic) IBOutlet UIImageView *mainMediaIcon;
@property (strong, nonatomic) IBOutlet UIToolbar *addItemToolBar;
@property (weak,nonatomic) NSArray *albums;
@property(weak,nonatomic) MediaManager * manager;
@property(weak,nonatomic) PhotoManager * photoManager;

@property (nonatomic) BOOL animateTransition ;
@property (nonatomic) BOOL presenting ;

@property CGPoint labelStartedPoint;
@property (strong, nonatomic) IBOutlet UILabel *titleMain;

@end
