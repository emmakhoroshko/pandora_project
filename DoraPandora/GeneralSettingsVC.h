//
//  GeneralSettingsVC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 14.09.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneralSettingsVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableViewSettings;

@end
