//
//  SettingsTableViewCell.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 14.09.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageForSetting;

@property (weak, nonatomic) IBOutlet UILabel *settingName;

@end
