//
//  ColorsManager.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 21.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "ColorsManager.h"

@implementation ColorsManager

+ (UIColor *)colorWithR:(CGFloat)red g:(CGFloat)green b:(CGFloat)blue alpha:(CGFloat)alpha {
    
    return [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:alpha];
}

+(UIColor *)mainNoteColor{
    return [self colorWithR:249 g:200 b:55 alpha:1];
}
+(UIColor *)iconsTintNoteColor{
    return [self colorWithR:11 g:22 b:34 alpha:1];
}
+(UIColor *)textFieldNoteColor{
    return [self colorWithR:11 g:22 b:34 alpha:1];
}
+(UIColor *)iconsTintPasswordColor{
    return [self colorWithR:11 g:22 b:34 alpha:1];
}

+(UIColor *)navigationBarIconsColor{
    return [self colorWithR:127 g:127 b:127 alpha:1];
}

+(UIColor *)viewsBorderColor{
    return [self colorWithR:127 g:127 b:132 alpha:1];
}
+(UIColor *)mainPasswordColor{
    
    return [self colorWithR:164 g:118 b:255 alpha:1];
}
+(UIColor *)buttonsColor{
    return [self colorWithR:82 g:120 b:235 alpha:1];
}

+(UIColor *)mainMediaColor{
    return [self colorWithR:117 g:209 b:255 alpha:1];
}
//+(UIColor *)buttonColor{
//    return [self colorWithR:134 g:132 b:132 alpha:1];
//}
+(UIColor *)correctPasswordColor{
    return [self colorWithR:45 g:123 b:12 alpha:1];
}
@end
