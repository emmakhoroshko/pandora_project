//
//  PasswordManager.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 22.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "PasswordManager.h"


@interface PasswordManager() {
   NSMutableArray *passwords;
    NSArray *sortedPasswords;
}

@end


@implementation PasswordManager


- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)saveInfo:(NotesModel*)password {
    
    
    NSManagedObject * pasObj = [NSEntityDescription insertNewObjectForEntityForName:@"Passwords" inManagedObjectContext:self.managedObjectContext];
    
    [pasObj setValue:password.pwTitle forKey:@"title"];
    [pasObj setValue:password.pwDate forKey:@"date"];
    [pasObj setValue:password.pwLogin  forKey:@"login"];
    [pasObj setValue:password.pwComment forKey:@"comment"];
    [pasObj setValue:password.pwAdditionalInfo forKey:@"ad_info"];
    [pasObj setValue:password.pwPassword forKey:@"password"];
    
    [self saveContext];
    [self initPass];
}


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            
            NSLog(@"Saving didn't work so well.. Error: %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void)deleteInfo:(NotesModel *)password {
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Passwords" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entityDescription];
    
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"(date==%@)and(password==%@)", password.pwDate,password.pwPassword];
    [request setPredicate:predicate];
    
    NSArray *objects =[context executeFetchRequest:request error:nil];
    for (NSManagedObject *obj in objects) {
        [context deleteObject:obj];
    }
    
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
   
    [self initPass];
    
    
}







-(SearchScreen_Cell_VC *)searchPasswordsContainString:(NSString*)string indexPath:(NSIndexPath *)index tableview: (UITableView *)tableView{
    SearchScreen_Cell_VC *cell =[tableView dequeueReusableCellWithIdentifier:@"SearchScreen_Cell_VC"];
//    NSManagedObjectContext *context = [self managedObjectContext];
//    
//    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Passwords" inManagedObjectContext:context];
//    NSFetchRequest *request = [[NSFetchRequest alloc] init];
//    [request setEntity:entityDescription];
//    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"(login CONTAINS[cd] %@)or(password  CONTAINS[cd] %@)or(title  CONTAINS[cd] %@)or(ad_info  CONTAINS[cd] %@)or(comment  CONTAINS[cd] %@)",string,string,string,string,string];
//    [request setPredicate:predicate];
//    
//    NSArray *objects =[context executeFetchRequest:request error:nil];
   NSArray *objects = [self searchPasswordsContainString:string];
    if (objects.count >0) {
        
        
    NotesModel *obj =objects[index.item];
        NSLog(@"%ld (long)index.item",(long)index.item);
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd.MM.yyyy"];
        NSString *dateString = [dateFormatter stringFromDate:obj.pwDate];

   
    cell.mainLabel.text =obj.pwLogin;
    cell.passwordIfExist.text=obj.pwPassword;
    cell.sectionNameLabel.text = @"Password";
    cell.passwordNameLabel.text = obj.pwTitle;
        cell.dateLabel.text = dateString;
    }
    return cell;
    
}

-(NSArray *)searchPasswordsContainString:(NSString*)string{
    NSMutableArray *array =
    [NSMutableArray arrayWithArray:passwords];
    
//    for ( NotesModel *password in  array) {
    
//    NSManagedObjectContext *context = [self managedObjectContext];
//    
//    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Passwords" inManagedObjectContext:context];
//    NSFetchRequest *request = [[NSFetchRequest alloc] init];
//    [request setEntity:entityDescription];
   
    // array now contains { @"Chris", @"Melissa" }or(title  CONTAINS[cd] '%@')or(additionalInfo  CONTAINS[cd] '%@')or(password  CONTAINS[cd] '%@')
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"(_pwComment CONTAINS[cd] %@)or(_pwPassword  CONTAINS[cd] %@)or(_pwAdditionalInfo CONTAINS[cd] %@)or(_pwLogin CONTAINS[cd] %@)",string,string,string,string];
        
   [array filterUsingPredicate:predicate];
    
    
    return array;
    
}

-(id)init {
    self =[super init];
    if(self) {
        
        [self initPass];
    }
    return self;
}


-(void)initPass{
    NSFetchRequest *request =[[NSFetchRequest alloc]initWithEntityName:@"Passwords"];
    
    
    passwords =[[NSMutableArray alloc]init];
    
    NSArray *tmpObj =[self.managedObjectContext executeFetchRequest:request error:nil];
    for ( NSManagedObject *obj in tmpObj) {
        NotesModel *password =[[NotesModel alloc]initPasswordWithTitle:[obj valueForKey:@"title"] login:[obj valueForKey:@"login"] password:[obj valueForKey:@"password"] additionalInfo:[obj valueForKey:@"ad_info"] comment:[obj  valueForKey:@"comment"] date:[obj valueForKey:@"date"]];
        
        [passwords addObject:password];
    }
    [self saveContext];
}

-(NSArray *)passwords {
    return passwords;
}


-(NSArray *)sortedPasswords {
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"pwDate"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    sortedPasswords = [passwords sortedArrayUsingDescriptors:sortDescriptors];
    return sortedPasswords;
}




-(void)addPasswordInfo:(NotesModel *)pass{
   
    [self saveInfo:pass];
    
}


-(BOOL)isObjectExistAtIndex:(NSInteger)index {
 if ([passwords objectAtIndex:index]) {
     return YES;
 }else{
        return  NO;
     }
    
}

-(void)deletePasswordatIndex:(NotesModel *)pass {
    
    [self deleteInfo:pass];
    }


-(NSString *)dateFormatted  {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    NSDate *today = [NSDate date];
    NSString *s = [dateFormatter stringFromDate:today];
    return s;
}
@end
