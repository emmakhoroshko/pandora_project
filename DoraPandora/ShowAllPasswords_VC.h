//
//  ShowAllPasswords_VC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 22.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PasswordManager.h"
#import "CommonViewController.h"

@interface ShowAllPasswords_VC : CommonViewController
@property (weak, nonatomic) IBOutlet UIImageView *mainPasswordIcon;
@property (weak, nonatomic) IBOutlet UIView *designView;
@property (weak, nonatomic) IBOutlet UITableView *passwordsTableView;
@property (weak, nonatomic) IBOutlet UIToolbar *addItemToolBar;
@property (strong,nonatomic) NSArray *passwords;
@property (strong,nonatomic) PasswordManager *pwManager;

@property (nonatomic) BOOL animateTransition ;
@property (nonatomic) BOOL presenting ;

@property CGPoint labelStartedPoint;
@property (strong, nonatomic) IBOutlet UILabel *titleMain;

@end
