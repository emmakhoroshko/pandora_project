//
//  ViewController.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 12.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"
#import "NoteManager.h"
#import "PasswordManager.h"
#import "MediaManager.h"
#import "PhotoManager.h"


@interface ViewController : CommonViewController

@property (weak, nonatomic) IBOutlet UIButton *settingsButton;


@property (weak, nonatomic) IBOutlet UIImageView *mediaIcon;
@property (weak, nonatomic) IBOutlet UIImageView *passwordIcon;
@property (weak, nonatomic) IBOutlet UIImageView *notesIcon;
@property (strong, nonatomic) IBOutlet UIButton *searchItems;

@property (weak, nonatomic) IBOutlet UIView *viewNote;

@property (weak, nonatomic) IBOutlet UIView *viewPassword;
@property (weak, nonatomic) IBOutlet UIView *viewMedia;

//@property (strong, nonatomic) IBOutlet NoteManager *manager;

//@property (weak, nonatomic) IBOutlet PasswordManager *pwManager;
//@property (weak, nonatomic) IBOutlet MediaManager *mediaManager;
//@property (weak, nonatomic) IBOutlet PhotoManager *photoManager;

//- (IBAction)openNotes:(id)sender;
@end

