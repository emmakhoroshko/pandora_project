//
//  SignUp_manager.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 11.08.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NotesModel.h"

@interface SignUp_manager : NSObject

-(NotesModel *)passwordInfo;
-(void)setPassword:(NotesModel *)password;
@end
