//
//  OnePhoto_VC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 22.06.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PhotoManager.h"
#import "AlbumModel.h"
#import "CommonPhotosVC.h"


@interface OnePhoto_VC : CommonPhotosVC
@property (strong, nonatomic) IBOutlet UIImageView *macroPhoto;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionPhotosView;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (nonatomic) int currentIndex;
//@property (strong, nonatomic) PhotoManager *manager;
//@property (strong, nonatomic) AlbumModel *photoBig;
//@property (nonatomic, strong) PHImageRequestOptions *requestOptions;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionMacroPhotosView;
@end
