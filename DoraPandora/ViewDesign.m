//
//  ViewDesign.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 15.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "ViewDesign.h"

@implementation ViewDesign

+(void)setRoundView:(UIView *)view andImage:(UIImageView *)image{    
    view.layer.cornerRadius = 18;
    image.layer.cornerRadius = (image.frame.size.width / 2);
    
}


@end
