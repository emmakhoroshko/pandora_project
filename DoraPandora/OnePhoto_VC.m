//
//  OnePhoto_VC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 22.06.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "OnePhoto_VC.h"
#import "ColorsManager.h"
#import "AlbumCell.h"
#import "AlbumModel.h"




@interface OnePhoto_VC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UINavigationControllerDelegate>

@end

@implementation OnePhoto_VC{
    NSArray *photosArray;
    int lastSelectedPhoto;
    int currentSelectedPhotoIndex;
    NSMutableArray *arrayForOneLineCollectionView;
    BOOL shouldScrollToBottom;
}
#pragma - mark - Life cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self createToolBar];
    
    shouldScrollToBottom = false;
    self.automaticallyAdjustsScrollViewInsets = YES;
    
    
    _collectionPhotosView.delegate = self;
    _collectionPhotosView.dataSource = self;
    _collectionMacroPhotosView.delegate = self;
    _collectionMacroPhotosView.dataSource = self;
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:true];
    [self updatePhotos];
    [self reloadCollectionViews];
    if (shouldScrollToBottom) {
        self.currentIndex = (int)[photosArray indexOfObject:photosArray.lastObject];
        shouldScrollToBottom = false;
    }
    [self scrolToItemOneLineCollectionView:self.currentIndex];
    [self scrolToItemMacroPhotoCollectionView:self.currentIndex];
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.manager setCurrentPhotoIndex:_currentIndex];

}

-(void)updatePhotos {
    [self.manager initPhotosArray];
    photosArray = [self.manager photoFromAlbum:self.albumName ];
    arrayForOneLineCollectionView = [self createArrayForOneLineCollectionView];
    
}

-(void)reloadCollectionViews{
    
    [_collectionPhotosView reloadData];
    [_collectionMacroPhotosView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - Tool Bar

- (void)createToolBar{
    
    UIBarButtonItem *newItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"newItem"] style:UIBarButtonItemStyleDone target:self action:@selector(addNewItemAction:)];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *deleteItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Delete-50"] style:UIBarButtonItemStyleDone target:self action:@selector(deleteItem:)];
    
    UIBarButtonItem *shareItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Upload-50"] style:UIBarButtonItemStyleDone target:self action:@selector(shareItem:)];
    NSArray *toolBarItems = [[NSArray alloc]initWithObjects:deleteItem,spacer, shareItem,spacer, newItem, nil];
    
    [_toolbar setItems:toolBarItems animated:YES];
    
}

#pragma - mark - Actions add, share, delete
- (IBAction)addNewItemAction:(id)sender
{
    [self callCommonDataPicker];
    shouldScrollToBottom = true;
}


- (IBAction)shareItem:(id)sender
{
    NSArray *activityItems = [[NSArray alloc]init];
    AlbumModel *currentImage =photosArray[self.currentIndex];
    
    activityItems = @[currentImage.image];
    
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    
    activityViewControntroller.excludedActivityTypes = @[];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        activityViewControntroller.popoverPresentationController.sourceView = self.view;
        activityViewControntroller.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/4, 0, 0);
    }
    
    [self presentViewController:activityViewControntroller animated:true completion:nil];
    
    
}


- (IBAction)deleteItem:(id)sender
{  //NSIndexPath *indPath = [NSIndexPath indexPathForItem:selectedPhotoIndex inSection:0];
    
    
    //  int index = (int)[[_manager photos] indexOfObject:_photosArray[selectedPhotoIndex]];
    
    [self.manager deletePhoto:photosArray[self.currentIndex]];
    [self updatePhotos];
    [self reloadCollectionViews];
}

#pragma - mark - NavigationBar

-(void)createNavigationBar{
    
    self.title =[NSString stringWithFormat:@"%d",self.currentIndex ];
    
    UIBarButtonItem *choose = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"done"] style:UIBarButtonItemStyleDone target:self action:@selector(chooseSeveral:)];
    choose.tintColor =[ColorsManager mainNoteColor];
    self.navigationItem.leftBarButtonItem.tintColor = [ColorsManager mainNoteColor];
    self.navigationItem.rightBarButtonItem = choose;
    
}
- (IBAction)chooseSeveral:(id)sender
{
}



#pragma - mark - CollectionView


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == _collectionPhotosView) {
        
        AlbumCell *cell =[_collectionPhotosView dequeueReusableCellWithReuseIdentifier:@"AlbumCell" forIndexPath:indexPath];
        
        
        AlbumModel *photo = arrayForOneLineCollectionView[indexPath.row];
        
        UIImage *image = [self.manager loadImage:photo.imageName];
        
        cell.photosOneLine.image = image;
        
        return cell;
        
        
    }else{
        AlbumCell *cell =[_collectionMacroPhotosView dequeueReusableCellWithReuseIdentifier:@"AlbumCellMacro" forIndexPath:indexPath];
        AlbumModel *photoMacro =photosArray[indexPath.row];
        cell.layer.masksToBounds = YES;
        UIImage *image = [self.manager loadImage:photoMacro.imageName];
        cell.photoMacroView_OnePhotoVC.image = image;
        return cell;
    }
}


-(NSMutableArray*)createArrayForOneLineCollectionView{
    NSMutableArray *array = [photosArray mutableCopy];
    for (int i =0; i<3; i++) {
        AlbumModel  *fakePhoto =[[AlbumModel alloc]init:nil defaultDate:nil albumName:self.albumName image_path:nil img_name:nil];
        
        [array addObject:fakePhoto];
        [array insertObject:fakePhoto atIndex:0];
    }
    
    return array;
    
}



-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView ==_collectionMacroPhotosView) {
        
        CGFloat itemWidth = [UIScreen mainScreen].bounds.size.width;
        CGFloat itemHeight =[UIScreen mainScreen].bounds.size.height;
        return CGSizeMake(itemWidth, itemHeight);
        
    }else{
        
        CGFloat itemHeight2 = _collectionPhotosView.bounds.size.height;
        return CGSizeMake(itemHeight2, itemHeight2);
        
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == _collectionPhotosView){
        NSMutableArray *array =[self createArrayForOneLineCollectionView];
        
        return array.count;
    }else{
        return photosArray.count;
        
        
    }}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}




-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView ==_collectionPhotosView) {
        if (((int)indexPath.row>=3)&((int)indexPath.row < photosArray.count+3)) {
            //+3 это кол-во вспомогательных пустых изображений, которые добавились для центрирования мелкой коллекшнвью при скроллинге бляяяяяя
            lastSelectedPhoto =self.currentIndex;
            self.currentIndex =(int)indexPath.row-3;//для нижней коллекшн вью я добавляю 3 в начало и 3 в конец фото что б по центру выбранные картинки при скроллинге были
            
            
            [self scrolToItemMacroPhotoCollectionView:self.currentIndex];
            [self scrolToItemOneLineCollectionView:self.currentIndex];
        }
        
    }else if (collectionView ==_collectionMacroPhotosView){
        lastSelectedPhoto =self.currentIndex;
        self.currentIndex =(int)indexPath.row;
        [self scrolToItemMacroPhotoCollectionView:self.currentIndex];
        [self scrolToItemOneLineCollectionView:self.currentIndex];
    }
    
}


-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    //  self.collectionMacroPhotosView.frame = [UIScreen mainScreen].bounds;
    //[UIScreen mainScreen].bounds;
    //[_collectionMacroPhotosView.contentMode =  ]
    
}

#pragma - mark - Scrolling methods



-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(scrollView == _collectionMacroPhotosView) {
        
        int tmpIndex = roundf(scrollView.contentOffset.x/[UIScreen mainScreen].bounds.size.width);
        self.currentIndex = tmpIndex;
        [self scrolToItemOneLineCollectionView:_currentIndex];
        
    }
    
}

-(void)scrolToItemOneLineCollectionView:(int)index{
    NSIndexPath *indPath2 = [NSIndexPath indexPathForItem:index+3 inSection:0];
    [_collectionPhotosView  scrollToItemAtIndexPath:indPath2 atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:TRUE];
    
}

-(void)scrolToItemMacroPhotoCollectionView:(int)index{
    NSIndexPath *indPath = [NSIndexPath indexPathForItem:index inSection:0];
    [_collectionMacroPhotosView scrollToItemAtIndexPath:indPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:FALSE];
    
}


#pragma - mark - selected Cell Borders marked

-(void)markNewCell:(int)selectedIndex{
    
    NSIndexPath *indexPathSelected =[NSIndexPath indexPathForItem:selectedIndex inSection:0];
    AlbumCell *cell =[_collectionPhotosView dequeueReusableCellWithReuseIdentifier:@"AlbumCell" forIndexPath:indexPathSelected];
    cell.layer.borderWidth = 3.0;
    cell.layer.borderColor =[UIColor blueColor].CGColor;
    
}

-(void)markSelectedCell:(int)selectedIndex{
    
    NSIndexPath *indexPathSelected =[NSIndexPath indexPathForItem:selectedIndex inSection:0];
    UICollectionViewCell *cell =[_collectionPhotosView cellForItemAtIndexPath:indexPathSelected];
    cell.layer.borderWidth = 3.0;
    cell.layer.borderColor =[UIColor blueColor].CGColor;
    
}

-(void)cancelCellBorderAtIndex:(int)index{
    NSIndexPath *cellIndex = [NSIndexPath indexPathForItem:index inSection:0];
    UICollectionViewCell *cell =[_collectionPhotosView cellForItemAtIndexPath:cellIndex];
    cell.layer.borderWidth =0;
    cell.layer.borderColor =nil;
    
}


#pragma - mark - Photo Picker

//-(void)newItem:(UIImage *)photo {
//
//    NSDate *now =[NSDate date];
//    AlbumModel * photoNew;
//    NSDateFormatter *dateForName =[[NSDateFormatter alloc]init];
//    dateForName.dateFormat=@"yyyyMMdd'T'HHmmss";
//
//    photoNew=[[AlbumModel alloc]init:photo defaultDate:now albumName:albumName image_path:nil img_name:[NSString stringWithFormat:@"%@%lu",[dateForName stringFromDate:now],(unsigned long)_photosArray.count]];
//
//    NSLog(@" _currentIndex   _currentIndex %d",_currentIndex);
//   // [self cancelCellBorderAtIndex:previousPhotoIndex];
//
//}

//-(void)callPhotoPicker {//добавить выбор нескольких фото, подключение библиотеки
//        CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
//
//    // set delegate
//    picker.delegate = self;
//    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance] setShadowImage:nil];
//    [self presentViewController:picker animated:YES completion:nil];
//
//}
//- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
//    self.requestOptions = [[PHImageRequestOptions alloc] init];
//    self.requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
//    self.requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
//
//    // this one is key
//    self.requestOptions.synchronous = true;
//    assets = [NSMutableArray arrayWithArray:assets];
//    PHImageManager *manager = [PHImageManager defaultManager];
// //   NSMutableArray *images = [NSMutableArray arrayWithCapacity:[assets count]];
//
//    // assets contains PHAsset objects.
//    __block UIImage *ima;
//
// // int  numbers = (int)assets.count;
//    for (PHAsset *asset in assets) {
//        // Do something with the asset
//
//        [manager requestImageForAsset:asset targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeDefault options:self.requestOptions resultHandler:^void(UIImage *image, NSDictionary *info) {
//            ima = image;
//        }];
//        [self newItem:ima];
//    }
//
//
// [self reloadDataCollectionViews];
//    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
//
// [self dismissViewControllerAnimated:YES completion:nil];
//}

//- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
//    [self dismissViewControllerAnimated:YES completion:nil];
//}

-(void)reloadDataCollectionViews{
    
    [self.manager initPhotosArray];
    
    photosArray = [self.manager photoFromAlbum:self.albumName ];
    //    self.currentIndex =(int)[_photosArray indexOfObject:[_photosArray lastObject]];
    arrayForOneLineCollectionView = [self createArrayForOneLineCollectionView];
    [_collectionMacroPhotosView reloadData];
    [_collectionPhotosView reloadData];
    //    [self scrolToItemMacroPhotoCollectionView:self.currentIndex];
    //    [self scrolToItemOneLineCollectionView:self.currentIndex];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
