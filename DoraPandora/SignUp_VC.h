//
//  SignUp_VC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 10.08.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignUp_manager.h"
#import "ColorsManager.h"
#import "SingUpInBase_VC.h"


@interface SignUp_VC : SingUpInBase_VC {
    
}

@property (strong, nonatomic) IBOutlet UILabel *labelSignUp;
@property (strong, nonatomic) IBOutlet UIImageView *imgReplyPassword;
@property (strong, nonatomic) IBOutlet UITextField *passwordReplyTextField;
@property (strong, nonatomic) IBOutlet UIView *linePasReply;
@property (strong, nonatomic) IBOutlet UIImageView *imgPassword;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIView *linePassword;
@property (strong, nonatomic) IBOutlet UIButton *buttonSignUp;
@property (strong, nonatomic) IBOutlet UIView *password_ViewTextField;
@property (strong, nonatomic) IBOutlet UIView *passwordReply_ViewTextField;

@end
