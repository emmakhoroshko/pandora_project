//
//  BiometricAuthenticationFacade.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 15.08.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "BiometricAuthenticationFacade.h"

#define IOS_VERSION [[UIDevice currentDevice].systemVersion intValue]

@implementation BiometricAuthenticationFacade




- (instancetype)init {
    self = [super init];
    if (self) {
        if (IOS_VERSION >= 8) {
            self.authenticationContext = [[LAContext alloc] init];
        }
    }
    return self;
}


- (BOOL)isPassByBiometricsAvailable {
    return [self.authenticationContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                                                   error:NULL];
}

- (void)passByBiometricsWithReason:(NSString *)reason
                       succesBlock:(void(^)())successBlock
                      failureBlock:(void(^)(NSError *error))failureBlock {
    [self.authenticationContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:reason reply:^(BOOL success, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success) {
                successBlock();
            } else {
                failureBlock(error);
            }
        });
    }];
}




-(void)authenticationMethod{
    LAContext *context = [[LAContext alloc] init];
    
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                localizedReason:@"Are you the device owner?"
                          reply:^(BOOL success, NSError *error) {
                              
                              //                              if (error) {
                              //                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                              //                                                                                  message:@"There was a problem verifying your identity."
                              //                                                                                 delegate:nil
                              //                                                                        cancelButtonTitle:@"Ok"
                              //                                                                        otherButtonTitles:nil];
                              //                                  [alert show];
                              //                                  return;
                              //                              }
                              //
                              //                              if (success) {
                              //                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                              //                                                                                  message:@"You are the device owner!"
                              //                                                                                 delegate:nil
                              //                                                                        cancelButtonTitle:@"Ok"
                              //                                                                        otherButtonTitles:nil];
                              //                                  [alert show];
                              //
                              //                              } else {
                              //                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                              //                                                                                  message:@"You are not the device owner."
                              //                                                                                 delegate:nil
                              //                                                                        cancelButtonTitle:@"Ok"
                              //                                                                        otherButtonTitles:nil];
                              //                                  [alert show];
                              //                              }
                              
                          }];
        
    } else {
        //        
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
        //                                                        message:@"Your device cannot authenticate using TouchID."
        //                                                       delegate:nil
        //                                              cancelButtonTitle:@"Ok"
        //                                              otherButtonTitles:nil];
        //        [alert show];
        
    }
    
}


@end
