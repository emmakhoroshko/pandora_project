//
//  SingleNote_VC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 16.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "SingleNote_VC.h"
#import "ShowAllNotes_VC.h"
#import "NotesModel.h"
#import "NoteManager.h"
#import "ColorsManager.h"

int const kSingleNoteDefaultBottomOffset = 48;


@interface SingleNote_VC (){
  
    NSString *textNote;
    BOOL wantDelete;
}

@end

@implementation SingleNote_VC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    textNote = _note.noteBody;
    _textNoteField.text = _note.noteBody;
    [self createNavigationBar];
    [self createToolBar];
    wantDelete = false;
    
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self registerKeyboardNotifications];
}


-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:true];
    if (!(wantDelete)) {
        
    
  NSString *text = _textNoteField.text;
            if (text.length==0) {
            NSLog(@"Do not save empty note");
                    }else {
            if (![text isEqualToString:textNote]) {
                _note.defaultDate = [NSDate date];
            }
            [self save];
       }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

    
    NSString *text = _textNoteField.text;
    if (text.length == 0) {
        NSLog(@"Do not save empty note");
       
    }else {
        if (![text isEqualToString:textNote]) {
        [self save];
     }
    }
    [self removeKeyboardNotifications];

}


#pragma mark - Navigation Bar

-(void)createNavigationBar{
    
    self.title = _note.noteTitle;
    
    UIBarButtonItem *save = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"done"] style:UIBarButtonItemStyleDone target:self action:@selector(saveNote:)];
    save.tintColor =[ColorsManager mainNoteColor];
    self.navigationItem.leftBarButtonItem.tintColor = [ColorsManager mainNoteColor];
    self.navigationItem.rightBarButtonItem = save;
    
}

#pragma mark - Tool Bar

- (void)createToolBar{
    
    UIBarButtonItem *newItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"newItem"] style:UIBarButtonItemStyleDone target:self action:@selector(createNewNoteAction:)];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *deleteItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Delete-50"] style:UIBarButtonItemStyleDone target:self action:@selector(deleteItem:)];
    
    UIBarButtonItem *shareItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Upload-50"] style:UIBarButtonItemStyleDone target:self action:@selector(shareItem:)];
    NSArray *toolBarItems = [[NSArray alloc]initWithObjects:deleteItem,spacer, shareItem,spacer, newItem, nil];
    
    [_toolBar setItems:toolBarItems animated:YES];
    
}

#pragma mark - Actions

- (IBAction)createNewNoteAction:(UIButton *)sender {
    [self save];
    NSString *date = [_manager dateFormatted:[NSDate date]];
   NSDate *now =[NSDate date];
    
    _note =[[NotesModel alloc]initWithTitle: NSLocalizedString(@"new_note", nil) noteBody:@"" date:date defaultDate:now];
    _textNoteField.text = _note.noteBody;
    [self createNavigationBar];
    [self createToolBar];
   // [_manager addNote:_note];
    _currentIndex =(int)[[_manager notes] indexOfObject:_note];
    
}

- (IBAction)saveNote:(UIButton *)sender{
    
    [_textNoteField endEditing:YES];
   // [self save];
}

-(void)save {
    
    NSString *text = _textNoteField.text;
    if(text.length>20) {
        _note.noteTitle = [NSString stringWithFormat:@"%@ ...",[text substringWithRange:NSMakeRange(0, 20)]];
        
    }else{
        _note.noteTitle = text;
    }
    _note.noteBody =_textNoteField.text;
    NSString *date = [_manager dateFormatted:[NSDate date]];
    _note.noteDate =[NSString stringWithFormat:@"%@",date];
    [_manager addNote:_note];
}

- (IBAction)deleteItem:(UIButton *)sender {
    
  //  NSString *text = _textNoteField.text;
    wantDelete = true;
        NSLog(@"%d",_currentIndex);
      //  [_manager deleteNote:_note];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)shareItem:(UIButton *)sender {
    
    NSString *noteString = _textNoteField.text;
    
    NSArray *activityItems = @[noteString];
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewControntroller.excludedActivityTypes = @[];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        activityViewControntroller.popoverPresentationController.sourceView = self.view;
        activityViewControntroller.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/4, 0, 0);
    }
    [self presentViewController:activityViewControntroller animated:true completion:nil];
    
}



#pragma mark - Keyborad notifications

- (void) registerKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

}


- (void) removeKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

#pragma mark - Keyborad appearance

-(void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    CGRect rawFrame      = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    
    _bottomOffset.constant = keyboardFrame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)keyboardWillHide {
    
    _bottomOffset.constant = kSingleNoteDefaultBottomOffset;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}


#pragma mark - Other methods




@end
