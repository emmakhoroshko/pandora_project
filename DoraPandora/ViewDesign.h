//
//  ViewDesign.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 15.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewDesign : UIView

+(void)setRoundView:(UIView *)view andImage:(UIImageView *)image;

@end
