//
//  PhotosVC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 21.06.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PhotoManager.h"
#import "MediaManager.h"
#import "AlbumModel.h"
//#import <CTAssetsPickerController/CTAssetsPickerController.h>
#import "CommonPhotosVC.h"


@interface PhotosVC : CommonPhotosVC

@property (strong, nonatomic) IBOutlet UICollectionView *photosCollectionView;

@property (strong, nonatomic) IBOutlet UIToolbar *photosToolBar;

//@property (strong,nonatomic) PhotoManager *manager;
@property (strong,nonatomic) MediaManager *mediaManager;
@property (nonatomic) int currentIndex;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

//@property (nonatomic, strong) PHImageRequestOptions *requestOptions;

//-(void)callPhotoPicker;

@end
