//
//  NoteManager.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 13.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NotesModel.h"
#import <CoreData/CoreData.h>
#import "SearchScreen_Cell_VC.h"

@interface NoteManager : NSObject
@property (strong, nonatomic, readonly) NSPersistentContainer *persistentContainer;

- (NSArray *)notes;
- (void)addNote:(NotesModel *)note;
-(void)deleteNote:(NotesModel * )note;
-(NSString *)dateFormatted:(NSDate *)date ;
-(NSArray *)sortedNotes;
-(SearchScreen_Cell_VC *)searchNoteContainString:(NSString*)string indexPath:(NSIndexPath *)index tableview: (UITableView *)tableView;
-(NSArray *)searchNotesContainString:(NSString*)string;
@end
