//
//  BiometricAuthenticationFacade.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 15.08.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import <UIKit/UIKit.h>

@interface BiometricAuthenticationFacade : NSObject
@property (nonatomic, strong) LAContext *authenticationContext;
- (BOOL)isPassByBiometricsAvailable;
- (void)passByBiometricsWithReason:(NSString *)reason
                       succesBlock:(void(^)())successBlock
                      failureBlock:(void(^)(NSError *error))failureBlock;
@end
