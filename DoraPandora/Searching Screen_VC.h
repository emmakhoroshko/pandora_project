//
//  Searching Screen_VC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 30.08.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchScreen_Cell_VC.h"
#import "NoteManager.h"
#import "PasswordManager.h"
#import "MediaManager.h"

@interface Searching_Screen_VC : UIViewController
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIView *designView;
@property (strong, nonatomic) IBOutlet UITableView *searchingResultsTableView;
@property (weak, nonatomic) IBOutlet PasswordManager *pwManager;
@property (weak, nonatomic) IBOutlet MediaManager *mediaManager;
@property (weak, nonatomic) IBOutlet NoteManager *noteManager;

@end
