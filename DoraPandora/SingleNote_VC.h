//
//  SingleNote_VC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 16.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoteManager.h"

@interface SingleNote_VC : UIViewController

@property (strong,nonatomic)  NotesModel *note;
@property (weak,nonatomic) NoteManager *manager;
@property (weak, nonatomic) IBOutlet UITextView *textNoteField;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (nonatomic) int currentIndex;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomOffset;



@end
