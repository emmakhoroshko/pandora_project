//
//  SettingsTableViewCell.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 14.09.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "SettingsTableViewCell.h"

@implementation SettingsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
