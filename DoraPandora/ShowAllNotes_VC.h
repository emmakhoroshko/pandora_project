//
//  ShowAllNotes_VC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 15.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoteManager.h"

@interface ShowAllNotes_VC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *pageIconNote;
@property (weak, nonatomic) IBOutlet UIView *designRoundView;
@property (weak, nonatomic) IBOutlet UINavigationBar *showAllNotesNavigationBar;
@property (weak, nonatomic) IBOutlet UITableView *notesTableView;
@property (strong,nonatomic) NSArray *allNotes;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBarNote;
@property (weak,nonatomic) NoteManager *manager;
@property (nonatomic) BOOL animateTransition ;
@property (nonatomic) BOOL presenting ;

@property CGPoint labelStartedPoint;
@property (strong, nonatomic) IBOutlet UILabel *titleMain;


@end
