//
//  PhotosVC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 21.06.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "PhotosVC.h"
#import "AlbumCell.h"
#import "OnePhoto_VC.h"



@interface PhotosVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UINavigationControllerDelegate>{
    NSArray *photos;
    NSMutableArray *selectedCellsIndexes;
}

@end

@implementation PhotosVC {
    BOOL  multiplySelection;
    NSMutableArray *selectedPhotos;
    BOOL shouldScrollToBottom;
}



#pragma - mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedPhotos =[[NSMutableArray alloc]init];
    
    //  [self setOriginalView];
    _photosCollectionView.delegate = self;
    _photosCollectionView.dataSource = self;
    //    [self createToolBar];
    //    [self createNavigationBar];
    [self updateOriginalView];
    shouldScrollToBottom = true;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updatePhotosArray:)
                                                 name:@"photosArrayWasChanged" object:nil];
 
    // multiplySelection = false;
}

//-(void)viewDidAppear:(BOOL)animated{
//    if (shouldScrollToBottom) {
//        self.currentIndex = (int) [photos indexOfObject:photos.lastObject];
//        printf("%d - currentIndex", _currentIndex);
//        shouldScrollToBottom = false;// при addNewItem и первом показе
//    }
//
//    [self scrollToCurrentIndex];
//}






-(void)viewWillAppear:(BOOL)animated{
    
    
       //  [_manager checkFiles];]
   //
    if (shouldScrollToBottom) {
        self.currentIndex = (int) [photos indexOfObject:photos.lastObject];
        printf("%d - currentIndex", _currentIndex);
        shouldScrollToBottom = false;// при addNewItem и первом показе
    }else if ([self.manager getCurrentPhotoIndex]!=0) {
        printf(" %d ==== self.manager getCurrentPhotoIndex",[self.manager getCurrentPhotoIndex]);
        _currentIndex = [self.manager getCurrentPhotoIndex];
    }
    
    
  dispatch_async (dispatch_get_main_queue (), ^{
        [self scrollToCurrentIndex];
});
    
}

- (void)updatePhotosArray:(NSNotification *)note {
     [self updateOriginalView];
}


-(void)updateOriginalView{
    multiplySelection = false;
    [self updatePhotos];
    [selectedPhotos removeAllObjects];
    //  [self cancelSelectionCells];
    [_photosCollectionView reloadData];
    [self createNavigationBar];
    [self createToolBar];
    
}

-(void)updatePhotos{
    [self.manager initPhotosArray];
    photos = [self.manager photoFromAlbum:self.albumName ];
    
}

-(void)resetPhotos{
    photos = nil;
    
}

-(void)scrollToCurrentIndex{
    
    NSIndexPath *indPath = [NSIndexPath indexPathForItem:_currentIndex inSection:0];
    [_photosCollectionView scrollToItemAtIndexPath:indPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:FALSE];
}




-(void)viewWillDisappear:(BOOL)animated{
    //       if (!(photos.count == 0)) {
    //       AlbumModel *firstPhoto = photos.firstObject;
    //        [_mediaManager setAlbumCover:firstPhoto.image forAlbumName:_albumName];
    //    }else{
    //       [_mediaManager setAlbumCover:[UIImage imageNamed:@"default_albumCover" ] forAlbumName:_albumName];
    //    }
    [super viewWillDisappear:true];
}

- (void)didReceiveMemoryWarning {
    selectedCellsIndexes = nil;
    [super didReceiveMemoryWarning];
    [self popoverPresentationController];
    
}





#pragma - mark - Collection View


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    AlbumCell *cell =[_photosCollectionView dequeueReusableCellWithReuseIdentifier:@"AlbumCell" forIndexPath:indexPath];
    
    AlbumModel *photo = photos[indexPath.row];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImage *imgFromPath = [self.manager loadImage:photo.imageName];
        cell.photo.image = imgFromPath;
    });
    
    cell.highlighted = NO;
    cell.layer.borderWidth = 0;
    return cell;
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


-(void)markSelectedCell:(int)selectedIndex{
    
    
    NSIndexPath *indexPathSelected =[NSIndexPath indexPathForItem:selectedIndex inSection:0];
    UICollectionViewCell *cell =[_photosCollectionView cellForItemAtIndexPath:indexPathSelected];
    NSLog(@"markSelectedCell %d",selectedIndex);
    cell.highlighted =YES;
    cell.layer.borderWidth = 3;
    cell.layer.borderColor =[UIColor blackColor].CGColor;
    
}




-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSLog(@"%lu photos.count",(unsigned long)photos.count);
    return photos.count ;
    
}



-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = 0;
    width = [UIScreen mainScreen].bounds.size.width/3-4;
    return CGSizeMake(width, width);
    
}

#pragma - mark - Segues

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (multiplySelection) {
        
        [_photosToolBar setUserInteractionEnabled:YES];
        _photosToolBar.tintColor=[UIColor blueColor];
        AlbumModel *photo = photos[indexPath.row];
        [selectedPhotos addObject:photo];
        NSLog(@"selectedPhotos indexPath %d",(int)indexPath.row);
        [self markSelectedCell:(int)indexPath.row];
        
    }else{
        
        [self performSegueWithIdentifier:@"OnePhoto" sender:indexPath];
        
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"OnePhoto"]) {
        
        OnePhoto_VC *vc =[segue destinationViewController];
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        AlbumModel *photo = photos[indexPath.row];
        vc.manager = self.manager;
        vc.albumName = photo.albumName;
        vc.currentIndex = (int)[photos indexOfObject:photos[indexPath.row]];
        
    }
}

#pragma - mark - ToolBar

- (void)createToolBar{
    UIBarButtonItem *newItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"newItem"] style:UIBarButtonItemStyleDone target:self action:@selector(addNewPhoto:)];
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    NSArray *toolBarItems = [[NSArray alloc]initWithObjects:spacer, newItem,spacer, nil];
    
    [_photosToolBar setItems:toolBarItems animated:YES];
    
}

-(void)setToolBarForSelectedPhotos{
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *deleteItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Delete-50"] style:UIBarButtonItemStyleDone target:self action:@selector(deleteItemPH:)];
    
    UIBarButtonItem *shareItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Upload-50"] style:UIBarButtonItemStyleDone target:self action:@selector(shareItemPH:)];
    
    NSArray *toolBarItems = [[NSArray alloc]initWithObjects:spacer,deleteItem,spacer, shareItem,spacer,  nil];
    
    [_photosToolBar setItems:toolBarItems animated:YES];
}

#pragma - mark - ToolBar actions
-(IBAction)addNewPhoto:(id)sender {
    
    [self callCommonDataPicker];
    shouldScrollToBottom = true;
    
}

-(IBAction)shareItemPH:(id)sender {
    
    NSMutableArray *activityItems = [[NSMutableArray alloc]init];
    
    for (int i=0; i<(int)selectedPhotos.count; i++) {
        AlbumModel *im = selectedPhotos[i];
        [activityItems addObject:im.image];
    }
    
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewControntroller.excludedActivityTypes = @[];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        activityViewControntroller.popoverPresentationController.sourceView = self.view;
        activityViewControntroller.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/4, 0, 0);
    }
    
    [self presentViewController:activityViewControntroller animated:true completion:nil];
    
    //[self setOriginalView];
}

-(IBAction)deleteItemPH:(id)sender {
    int numbers = (int)selectedPhotos.count;
    for (int i=0; i<numbers; i++) {
        int indexPath =(int)[[self.manager photos] indexOfObject:selectedPhotos[i]];
        [self.manager deletePhoto:selectedPhotos[i]];
        NSLog(@"deleted it %d",indexPath);
    }
   // [self updateOriginalView];
    
}


#pragma - mark - NavigationBar

-(void)createNavigationBar{
    self.title = self.albumName;
    UIBarButtonItem *selectItems = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"done"] style:UIBarButtonItemStyleDone target:self action:@selector(selectItems:)];
    self.navigationItem.rightBarButtonItem = selectItems;
    
}

- (IBAction)selectItems:(UIButton *)sender{
    //перезагрузить  collectionView со скрытым кружком и при нажатии красить кружок в зеленый и добавлять фото в массив для дальшейшего действия
    [self setToolBarForSelectedPhotos];
    
    [_photosToolBar setUserInteractionEnabled:NO];
    _photosToolBar.tintColor=[UIColor lightGrayColor];
    
    multiplySelection = true;
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"cancel", nill) style:UIBarButtonItemStyleDone target:self action:@selector(cancelSelection:)];
    self.navigationItem.rightBarButtonItem = cancel;
    
}

- (IBAction)cancelSelection:(UIButton *)sender {
    [self updatePhotos];
}

-(void)chooseSeveralItems{
    
    
}

@end
