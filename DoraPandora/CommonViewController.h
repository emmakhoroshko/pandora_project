//
//  CommonViewController.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 03.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardManager_VC.h"
#import "PhotoManager.h"


@interface CommonViewController : KeyboardManager_VC


-(void) createnNavigationBarWithoutBorders;
-(void) makeNavigationBarHidden;
-(void) createNavigationBar;

@end
