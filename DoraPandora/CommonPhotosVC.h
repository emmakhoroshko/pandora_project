//
//  CommonPhotosVC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 04.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoManager.h"
#import <CTAssetsPickerController/CTAssetsPickerController.h>

@interface CommonPhotosVC : UIViewController

@property (nonatomic, strong) PHImageRequestOptions *requestOptions;
@property (strong,nonatomic) PhotoManager *manager;
@property (strong,nonatomic) NSString * albumName;


-(void)callCommonDataPicker;

@end
