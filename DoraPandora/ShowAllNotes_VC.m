//
//  ShowAllNotes_VC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 15.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "ShowAllNotes_VC.h"
#import "NoteManager.h"
#import "NoteTableViewCell.h"
#import "ViewDesign.h"
#import "SingleNote_VC.h"
#import "ColorsManager.h"
#import "ViewController.h"
#import "Animation_VC.h"
#define duration 0.5

@interface ShowAllNotes_VC () <UITableViewDataSource,UITableViewDelegate,UIViewControllerTransitioningDelegate>
//CheckoutPresentationControllerDelegate>
{
    
    NSDictionary *currentNote;
    NotesModel *newNote;
    double screenCenter;
    Animation_VC *animation;
}
@end

@implementation ShowAllNotes_VC

#pragma mark - Life cycle


- (void)viewDidLoad {
    [super viewDidLoad];
    _allNotes = [_manager sortedNotes];
    _notesTableView.delegate =self;
    _notesTableView.dataSource = self;
    _notesTableView.hidden =true;
    _designRoundView.hidden =true;
    [ViewDesign setRoundView:_designRoundView andImage:_pageIconNote];
    _pageIconNote.image = [UIImage imageNamed:@"note_icon"];
  
    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    
     animation=[[Animation_VC alloc]init];
    _titleMain = [[UILabel alloc]init];
    [self initLabel];

  //  _titleMain.bounds.size =[siz] CGSizeMake(142, 21);
   
    
    
   }

- (void)viewWillAppear:(BOOL)animated {
   
    [super viewWillAppear:animated];
    _allNotes = [_manager sortedNotes];
    [_notesTableView reloadData];
   //    [self setControllerHeight];
   
    [self animateAppearance];
    [self createNavigationBar];
    
    if (_animateTransition) {
        [self.navigationController setNavigationBarHidden:true];
        
    }

    [self createToolBar];
   }

-(void)initLabel{
    
  _titleMain=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150 , 53)];
   _titleMain.textColor = [UIColor blackColor];
   _titleMain.userInteractionEnabled=NO;
    _titleMain.text= NSLocalizedString(@"notes", nil);
    _titleMain.font =[UIFont systemFontOfSize:18];
    _titleMain.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_titleMain];
    //_labelStartedPoint =CGPointMake(114, 49);
}

#pragma mark - Animation

-(void)animateAppearance{
    
    if (_animateTransition) {
                 animation.presenting =_presenting;
       
        if (_presenting) {
            
                    _notesTableView.hidden = false;
                    _designRoundView.hidden = false;
                    _notesTableView.layer.opacity =0;
                    _designRoundView.layer.opacity =0;
            
        }
        
    [animation animateAppearance:self.view title:_titleMain icon:_pageIconNote];
   
[UIView animateWithDuration:duration delay:0.0 options:0.0 animations:^{
    
    _designRoundView.layer.opacity =_presenting ? 1:0;
    _notesTableView.layer.opacity =_presenting ? 1:0;
    
    
} completion:^(BOOL finished) {
    if (_presenting) {
       [self.navigationController setNavigationBarHidden:false];
    }
}];
        

   }

}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_allNotes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NoteTableViewCell *cell = [_notesTableView dequeueReusableCellWithIdentifier:@"NoteTableViewCell"];
    
    NotesModel *note = _allNotes[indexPath.row];
    cell.labelNoteCell.text = note.noteTitle;
    cell.labelNoteDateCell.text =note.noteDate;
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
     [ self performSegueWithIdentifier:@"showOneNoteSegue" sender:indexPath];
     [_notesTableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"%@",indexPath);
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    _animateTransition = false;
 
    if ([segue.identifier isEqualToString:@"showOneNoteSegue"]) {
         SingleNote_VC *vc =[segue destinationViewController];
        vc.manager =_manager;
        
        if (sender){
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        vc.note = _allNotes[indexPath.item];
        vc.currentIndex = (int)[[_manager  notes] indexOfObject:_allNotes[indexPath.item]];
       NSLog(@"%d vc.currentIndex 0",vc.currentIndex);
            [_manager deleteNote:_allNotes[indexPath.item]];
        }else{
            
            [self newNote];
            vc.note = newNote;
            vc.currentIndex =(int)[[_manager  notes] indexOfObject:newNote];
            NSLog(@"%d vc.currentIndex 1",vc.currentIndex);
        }
    
    }
}
#pragma mark - Navigation Bar

-(void)createNavigationBar{
    
    
  
   

//
    
          UIBarButtonItem *search = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"search"] style:UIBarButtonItemStyleDone target:self action:@selector(searchItem:)];
          UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu-50"] style:UIBarButtonItemStyleDone target:self action:@selector(backButton:)];
          [self.navigationController setNavigationBarHidden:false];
          
 
          
        // self.navigationItem.titleView.center =CGPointMake(0, 0);
  
          self.navigationItem.rightBarButtonItem = search;
       self.navigationItem.leftBarButtonItem = back;

//        self.navigationItem.leftBarButtonItem.customView.transform = (_presenting)? CGAffineTransformIdentity : CGAffineTransformIdentity;
        
}
- (IBAction)searchItem:(UIButton *)sender{
    
    
    
    
    
    
}

-(IBAction)backButton:(id)sender{
     _animateTransition = true;
   _presenting =false;
    [self.navigationController setNavigationBarHidden:true];
    
    [self animateAppearance];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];

}
#pragma mark - Tool Bar


- (IBAction)createNewNoteAction:(UIButton *)sender {
    
  
   [self performSegueWithIdentifier:@"showOneNoteSegue" sender:nil];
    
}

- (void)createToolBar{
    UIBarButtonItem *newItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"newItem"] style:UIBarButtonItemStyleDone target:self action:@selector(createNewNoteAction:)];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    NSArray *toolBarItems = [[NSArray alloc]initWithObjects:spacer,newItem,spacer, nil];
    [_toolBarNote setItems:toolBarItems animated:YES];
    
}

#pragma mark -newNote


-(void)newNote{
    
    NSDate *now =[NSDate date];
    NSLog(@"%@", now);
    
    NSString *date = [_manager dateFormatted:[NSDate date]];
    newNote = [[NotesModel alloc] initWithTitle:NSLocalizedString(@"new_note", nil) noteBody:@""  date:date defaultDate:now];
   // [_manager addNote:newNote];
   
}



#pragma mark -Design



- (void)showPreViewNotesAtIndex:(int)noteIndex {
    {
        
        
    }
}



/*
 #pragma mark - Navigation
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
 if (!cell)
 {
 cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
 reuseIdentifier:@"cell"];
 }
 
 cell.textLabel.text = currentAlbumData[@"titles"][indexPath.row];
 cell.detailTextLabel.text = currentAlbumData[@"values"][indexPath.row];
 
 return cell;
 }
 
 
 - (void)showDataForAlbumAtIndex:(int)albumIndex
 {
 // Защита от дурака: убедимся, что запрошенный индекс меньше числа альбомов
 if (albumIndex < allAlbums.count)
 {
 // Берём альбом:
 Album * album = allAlbums[albumIndex];
 // Сохраняем данные альбома, чтобы позже показать в TableView:
 currentAlbumData = [album tr_tableRepresentation];
 }
 else
 {
 currentAlbumData = nil;
 }
 
 // У нас есть данные, которые нам нужны. Обновляем TableView
 [dataTable reloadData];
 }
 */
@end
