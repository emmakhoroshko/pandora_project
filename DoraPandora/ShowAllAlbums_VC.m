//
//  ShowAllAlbums_VC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 16.06.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "ShowAllAlbums_VC.h"
#import "ViewDesign.h"
#import "AlbumCell.h"
#import "AlbumModel.h"
#import "PhotosVC.h"
#import "Animation_VC.h"
#define duration 0.5

@interface ShowAllAlbums_VC ()<UICollectionViewDelegate,UICollectionViewDataSource>

@end

@implementation ShowAllAlbums_VC {
    NSString *newAlbumName;
    BOOL  multiplySelection;
    NSMutableArray *selectedAlbums;
    
    double screenCenter;
    Animation_VC *animation;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedAlbums =[[NSMutableArray alloc]init];
    _photoCollectionView.delegate = self;
    _photoCollectionView.dataSource = self;
    
    [ViewDesign setRoundView:_designRoundView andImage:_mainMediaIcon];
   // [self setOriginalView];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    
    animation=[[Animation_VC alloc]init];
    _titleMain = [[UILabel alloc]init];
    [self initLabel];
   
    
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
     [self setOriginalView];
    [_photoManager setCurrentPhotoIndex:0];
    if (_animateTransition) {
        [self.navigationController setNavigationBarHidden:true];
        
    }
    [self animateAppearance];
    [super viewWillAppear:true];
    
    
    
    
}

#pragma - mark - ToolBar

- (void)createToolBar{
    UIBarButtonItem *newItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"newItem"] style:UIBarButtonItemStyleDone target:self action:@selector(addNewAlbum:)];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
   
    NSArray *toolBarItems = [[NSArray alloc]initWithObjects:spacer,newItem,spacer, nil];
    [_addItemToolBar setItems:toolBarItems animated:YES];
    
}
- (void)toolBarforDelete{
       UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *deleteItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Delete-50"] style:UIBarButtonItemStyleDone target:self action:@selector(deleteItem:)];
    NSArray *toolBarItems = [[NSArray alloc]initWithObjects:spacer,deleteItem,spacer, nil];
    [_addItemToolBar setItems:toolBarItems animated:YES];
    
}
-(IBAction)addNewAlbum:(id)sender {
   // [self performSegueWithIdentifier:@"" sender:nil];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"new_album", nil) 
                message:nil   preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"write_album_name", nil);
    }];
    
    
    UIAlertAction * newAlbumNameCreated = [UIAlertAction actionWithTitle:NSLocalizedString(@"create_album", nil) style:UIAlertActionStyleDefault
        handler:^(UIAlertAction * newAlbum) {
        newAlbumName =((UITextField *)[alert.textFields objectAtIndex:0]).text;
            NSDate *now =[NSDate date];
            AlbumModel *album = [[AlbumModel alloc]initAlbumCover:[UIImage imageNamed:@"default_albumCover" ] albumDate:now aName:newAlbumName];
            [_manager addAlbum:album];
            [self setOriginalView];

        }];
    [alert addAction: newAlbumNameCreated];
    
    
    UIAlertAction * cancelAdditingAlbum = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {} ];
    
    
    [alert addAction:cancelAdditingAlbum];
    
    [self presentViewController:alert animated:YES completion:nil];
    
   
    
}

-(IBAction)deleteItem:(id)sender {
    int numbers = (int)selectedAlbums.count;
    for (int i=0; i<numbers; i++) {
        AlbumModel *album =selectedAlbums[i];
       
        [_manager deleteAlbumAtIndex:selectedAlbums[i]];
        
        for (AlbumModel *photo in [_photoManager photoFromAlbum:album.aName]){
          [_photoManager deletePhoto:photo];
            
        }
        
        
        
      //  удалить все фото этого альбома
// тут должно вылезать окно "ВЫ  действительно хотите все нахер удалить"
        
}

     [self setOriginalView];
}


-(void)animateAppearance{
    
    if (_animateTransition) {
        animation.presenting =_presenting;
        if (_presenting) {
            
            _photoCollectionView.hidden = false;
            _designRoundView.hidden = false;
            _photoCollectionView.layer.opacity =0;
            _designRoundView.layer.opacity =0;
            
        }
        
        [animation animateAppearance:self.view title:_titleMain icon:_mainMediaIcon];
        
        [UIView animateWithDuration:duration delay:0.0 options:0.0 animations:^{
            
            _designRoundView.layer.opacity =_presenting ? 1:0;
            _photoCollectionView.layer.opacity =_presenting ? 1:0;
            
            
        } completion:^(BOOL finished) {
            if (_presenting) {
                [self.navigationController setNavigationBarHidden:false];
            }
        }];
        
        
    }
    
}

-(void)initLabel{
    
    _titleMain=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150 , 53)];
    _titleMain.textColor = [UIColor whiteColor];
    _titleMain.userInteractionEnabled=NO;
    _titleMain.text= NSLocalizedString(@"media", nil);
    _titleMain.font =[UIFont systemFontOfSize:18];
    _titleMain.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_titleMain];
    //_labelStartedPoint =CGPointMake(114, 49);
}


-(void)setOriginalView{
    [self createToolBar];
    [self createNavigationBar];
    
    multiplySelection = false;
     _albums =[_manager sortedAlbums];
    [selectedAlbums removeAllObjects];
   [_photoCollectionView reloadData];
}


#pragma - mark - NavigationBar

-(void)createNavigationBar{
   
    UIBarButtonItem *choose = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"done"] style:UIBarButtonItemStyleDone target:self action:@selector(chooseItems:)];
    self.navigationItem.rightBarButtonItem = choose;
    UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu-50"] style:UIBarButtonItemStyleDone target:self action:@selector(backButton:)];
    [self.navigationController setNavigationBarHidden:false];
    
        
    // self.navigationItem.titleView.center =CGPointMake(0, 0);
    
    self.navigationItem.leftBarButtonItem = back;
    
}
-(IBAction)backButton:(id)sender{
    _animateTransition =true;
    _presenting =false;
    [self.navigationController setNavigationBarHidden:true];
    
    [self animateAppearance];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
}


- (IBAction)chooseItems:(UIButton *)sender{
    
    [self toolBarforDelete];
    [_addItemToolBar setUserInteractionEnabled:NO];
    _addItemToolBar.tintColor=[UIColor lightGrayColor];
    multiplySelection= true;
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc]initWithTitle: NSLocalizedString(@"cancel", nil) style:UIBarButtonItemStyleDone target:self action:@selector(cancelSelection:)];
    self.navigationItem.rightBarButtonItem = cancel;
    
    
}
-(IBAction)cancelSelection:(id)sender{
    
    [self setOriginalView];
}


#pragma - mark - CollectionView


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
  
    AlbumCell *cell = [_photoCollectionView dequeueReusableCellWithReuseIdentifier:@"AlbumCell" forIndexPath:indexPath];
    
    
       AlbumModel *alb = _albums[indexPath.row];
    cell.album.image = alb.albumCover;
    cell.album.clipsToBounds = YES;
    cell.album.layer.cornerRadius = 4;
    cell.albumName.text =alb.aName;
    cell.layer.borderWidth = 0;
        return cell;
    
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_albums count];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (multiplySelection) {
        [_addItemToolBar setUserInteractionEnabled:YES];
        _addItemToolBar.tintColor=[UIColor blueColor];
        AlbumModel *album = _albums[indexPath.row];
        [selectedAlbums addObject:album];
       [self markSelectedCell:(int)indexPath.row];
        
    }else{
    
    
    [self performSegueWithIdentifier:@"PhotosVC" sender:indexPath];
    }
    
}

-(void)markSelectedCell:(int)selectedIndex{// когда обвожу cell нужно что бы она блокиовалась для дальнейших действий
    
    
    NSIndexPath *indexPathSelected =[NSIndexPath indexPathForItem:selectedIndex inSection:0];
    UICollectionViewCell *cell =[_photoCollectionView cellForItemAtIndexPath:indexPathSelected];
    cell.highlighted =YES;
   // cell.backgroundColor =[UIColor lightGrayColor];
    cell.layer.borderWidth = 2;
    cell.layer.borderColor =[UIColor lightGrayColor].CGColor;
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     _animateTransition =false;
    if ([segue.identifier isEqualToString:@"PhotosVC"]) {
        
        PhotosVC *vc =[segue destinationViewController];
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        AlbumModel *alb =_albums[indexPath.row];
        vc.albumName = alb.aName;
        vc.manager =_photoManager;
        vc.mediaManager =_manager;
       
        
    }
}

@end
