//
//  OnePassword_VC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 25.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "OnePassword_VC.h"

int const kOnePasswordDefaultBottomOffset2 = 46;

@interface OnePassword_VC (){
    BOOL textFieldsWasChanged;
    
}
@end

@implementation OnePassword_VC



#pragma mark - Life Cyrcle


- (void)viewDidLoad {
    [super viewDidLoad];
    _pw_comment.delegate = self;
    _pw_addInfo.delegate = self;
    _pw_name.delegate = self;
    _pw_login.delegate = self;
    _pw_password.delegate = self;
    _scrollView.delegate = self;
    
    
    [self createToolBar];
    [self createNavigationBar];
    [self setPwLabels];
    
    //первая буква
    NSString *text =_password.pwTitle;
    _pw_firstLetter.text =[[text substringWithRange:NSMakeRange(0,1)] uppercaseString];
    _pw_roundView.layer.cornerRadius =  _pw_roundView.layer.bounds.size.height/2;
    
    [self registerNotification];
    textFieldsWasChanged = NO;
}

-(void)setPwLabels {
    if (!(_newPasswordExample)) {
        _pw_name.text = _password.pwTitle;
        [self ableToUseToolbar];
    }else{
        _pw_name.text = @"";
        [self disableToUseToolBar];
    }
    _pw_login.text = _password.pwLogin;
    _pw_password.text = _password.pwPassword;
    _pw_addInfo.text =_password.pwAdditionalInfo;
    _pw_comment.text = _password.pwComment;
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    if ([_pw_login.text isEqualToString:@""]||[_pw_password.text isEqualToString:@""]) {
        [_manager deletePasswordatIndex:_password];
    }else{
        if ([_pw_name.text isEqualToString:@""]) {
            _pw_name.text = [NSString stringWithFormat:@"%@ №%d", NSLocalizedString(@"password", nil),_currentIndex+1];
        }
        [self save];
    }
    [self removeKeyboardNotifications];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [self checkCorrectPassword];
    [super viewWillDisappear:true];
}

-(void)checkCorrectPassword {
    if ([_pw_login.text isEqualToString:@""]||[_pw_password.text isEqualToString:@""]) {
        //  [_manager deletePasswordatIndex:_password];
    }else{
        if ([_pw_name.text isEqualToString:@""]) {
            _pw_name.text =  [NSString stringWithFormat:@"%@ №%d", NSLocalizedString(@"password", nil),_currentIndex+1]; }
        if (textFieldsWasChanged) {
            // [self save];
        }
    }
    
}

#pragma mark - KeyBoard Notifications

- (void) registerNotification {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)keyboardWillShow:(NSNotification *) notification {
    
    NSValue *value = [notification.userInfo objectForKey: UIKeyboardFrameEndUserInfoKey];
    CGRect keyEndFrame = [value CGRectValue];
    // CGSize keySize =keyEndFrame.size;
    CGRect keyboardFrame = [self.view convertRect:keyEndFrame fromView:nil];
    _bottomOffset.constant = keyboardFrame.size.height;
    
    [UIView animateWithDuration:0.8 animations:^{
        [self.view layoutIfNeeded];
    }];
    
}



-(void)keyboardWillHide:(NSNotification *) notification {
    
    _bottomOffset.constant = kOnePasswordDefaultBottomOffset2;
    
    [UIView animateWithDuration:0.8 animations:^{
        [self.view layoutIfNeeded];
    }];
    
}

- (void) removeKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"work textfield %@",textField);
    if ([textField isEqual:_pw_addInfo]||[textField isEqual:_pw_comment]){
        activeField =textField;
    }else{
        activeField=nil;
    }
    textFieldsWasChanged =YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
}


- (IBAction)backgroundTap:(UIControl *)sender {
    
    [self hideKeyboard];
    
}

-(void)hideKeyboard{
    
    [self.pw_name resignFirstResponder];
    [self.pw_login resignFirstResponder];
    [self.pw_password resignFirstResponder];
    [self.pw_addInfo resignFirstResponder];
    [self.pw_comment resignFirstResponder];
    
}

#pragma mark - ToolBar
- (void)createToolBar{
    
    UIBarButtonItem *newItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"newItem"] style:UIBarButtonItemStyleDone target:self action:@selector(createNewPassword:)];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *deleteItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Delete-50"] style:UIBarButtonItemStyleDone target:self action:@selector(deleteItem:)];
    
    UIBarButtonItem *shareItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Upload-50"] style:UIBarButtonItemStyleDone target:self action:@selector(shareItem:)];
    NSArray *toolBarItems = [[NSArray alloc]initWithObjects:deleteItem,spacer, shareItem,spacer, newItem, nil];
    
    [_toolbar setItems:toolBarItems animated:YES];
    
}

-(void)disableToUseToolBar {
    _toolbar.userInteractionEnabled = NO;
    _toolbar.tintColor =[UIColor lightGrayColor];
}
-(void)ableToUseToolbar {
    _toolbar.userInteractionEnabled = YES;
    _toolbar.tintColor =[UIColor blackColor];
    
}

-(IBAction)createNewPassword:(id)sender {
    
    NSString *date =[_manager dateFormatted];
    NSDate *now =[NSDate date];
    _password =[[NotesModel alloc]initPasswordWithTitle:NSLocalizedString(@"new_password", nil)  login:@"" password:@"" additionalInfo:@"" comment:date date: now ];
    [_manager addPasswordInfo:_password];
    _newPasswordExample =YES;
    [self createNavigationBar];
    // [self createToolBar];
    [self setPwLabels];
    _currentIndex =(int)[[_manager passwords] indexOfObject:_password];
    
}

-(IBAction)deleteItem:(id)sender {
    
    [_manager deletePasswordatIndex:_password];
    [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)shareItem:(id)sender{
    [self share:sender];
}

- (void)share:(id)sender {
    
    NSString *noteString = [NSString stringWithFormat: @"%@: %@\n%@: %@",NSLocalizedString(@"login", nil),  _pw_login.text, NSLocalizedString(@"password", nil), _pw_password.text];
    
    NSArray *activityItems = @[noteString];
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewControntroller.excludedActivityTypes = @[];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        activityViewControntroller.popoverPresentationController.sourceView = self.view;
        activityViewControntroller.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/4, 0, 0);
    }
    [self presentViewController:activityViewControntroller animated:true completion:nil];
    
}
#pragma - mark - NavigationBar

-(void)createNavigationBar {
    
    self.title = _password.pwTitle;
    UIBarButtonItem *save = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"done"] style:UIBarButtonItemStyleDone target:self action:@selector(savePassword:)];
    self.navigationItem.rightBarButtonItem = save;
    
}
-(IBAction)savePassword:(id)sender {
    
    [self checkCorrectPassword];
    [self save];
    [self ableToUseToolbar];
    [self hideKeyboard];
}

-(void)save{
    
    
    
    _password.pwTitle =_pw_name.text;
    _password.pwLogin =_pw_login.text;
    _password.pwPassword =_pw_password.text;
    _password.pwAdditionalInfo =_pw_addInfo.text;
    _password.pwComment = _pw_comment.text;
    _password.pwDate =[NSDate date];
    [_manager addPasswordInfo:_password];
    
}


@end
