//
//  Transition_VC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 31.07.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "Transition_VC.h"


@implementation Transition_VC

#define kTransitionDuration 0.35
#define duration  0.55
#define animDelay 0

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    return duration;
}



- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
   
   UIViewController * toController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
  
    UIViewController* fromController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    _container = [transitionContext containerView];
  //  UIView *finishView =[[UIView alloc]init];
    UIView *secondController = (_presenting) ? toController.view :fromController.view ;
       CGRect initialFrame = (_presenting) ? _originFrame : secondController.frame;
    
    CGRect finalFrame = _presenting ? secondController.frame : _originFrame ;
    
    double xScaleFactor = _presenting ?
    
    initialFrame.size.width / finalFrame.size.width :
    finalFrame.size.width / initialFrame.size.width;
    
    double yScaleFactor = _presenting ?
    
    initialFrame.size.height / finalFrame.size.height :
    finalFrame.size.height / initialFrame.size.height;
    
    //
    
    CGAffineTransform scaleTransform = CGAffineTransformScale(CGAffineTransformIdentity , xScaleFactor, yScaleFactor);
    
    if (_presenting) {
        secondController.transform = scaleTransform;
        secondController.center = CGPointMake(initialFrame.origin.x+initialFrame.size.width/2,initialFrame.origin.y+initialFrame.size.height/2);
        
          }
    
   
   
   [_container addSubview:toController.view];
   [_container bringSubviewToFront:secondController];
  //  [self animateMovingRightViews];
    
         [UIView animateWithDuration:duration delay:animDelay options:0.0 animations:^{
        
       // secondController.layer.opacity =(_presenting)? 1.0:0.0;
        secondController.transform = (_presenting)? CGAffineTransformIdentity :scaleTransform;
        secondController.center = CGPointMake(finalFrame.origin.x+finalFrame.size.width/2, finalFrame.origin.y+finalFrame.size.height/2);
    }completion:^(BOOL finished) {
                  [transitionContext completeTransition:finished];
        
     
    }];
    
    
    
}
 
    
-(void)animateMovingRightViews{
    
    for (UIView *view in _animatViews.reverseObjectEnumerator) {
          [self.container addSubview:view];
        
          //  [ bringSubviewToFront:view];
        NSLog(@"%f view.frame 8",view.frame.origin.x);
        
    [UIView animateWithDuration:duration delay:0.0 options:0.0 animations:^{
        
        
            if (_presenting) {
               
                view.center =CGPointMake(view.center.x+500, view.center.y);
            }else{
            
            view.center = CGPointMake(view.center.x-500, view.center.y);
            }
        }completion:^(BOOL finished) {
            
        }];

    }
}
    





//- (void) addView {
//    
//    [self.container addSubview:_passView1];
//    [self.container addSubview:_noteView1];
//
//}



- (void) showBlurView {
    
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    
    _visualEffectView = [[UIVisualEffectView alloc] init];
    _visualEffectView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    _visualEffectView.effect = nil;
    [self.container addSubview:_visualEffectView];
   // [self setConstraints:_visualEffectView];
    
    [UIView animateWithDuration:0.3 animations:^{
        _visualEffectView.effect = blurEffect;
    }];
}
- (void) hideBlurView {
    
    [self hideBlurView:^(BOOL finished) {}];
}


- (void)hideBlurView:(void (^ __nullable)(BOOL finished))completion {
    
    [UIView animateWithDuration:0.6 animations:^{
        _visualEffectView.effect = nil;
    } completion:^(BOOL finished) {
        completion(finished);
        if(finished)
            [_visualEffectView removeFromSuperview];
    }];
    
}

@end
