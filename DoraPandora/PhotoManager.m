//
//  PhotoManager.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 20.06.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "PhotoManager.h"

@implementation PhotoManager{
    NSMutableArray * photos;
    NSArray * sortedPhotos;
    int currentIndexOfphoto;
}



-(NSManagedObjectContext*)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context =[delegate managedObjectContext];
    }
    return context;
}


-(void)saveContext{
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    if (context != nil) {
        if ([context hasChanges] && ![context save:&error]) {
            
            NSLog(@"Saving didn't work so well.. Error: %@, %@", error, [error userInfo]);
            abort();
        }

     }
}





-(void)saveInfo:(AlbumModel *) photo{
    
    NSManagedObject *obj =[NSEntityDescription insertNewObjectForEntityForName:@"Photos" inManagedObjectContext:self.managedObjectContext];
    
    
    NSString *imgPath =[self pathOfSavingImgToDocuments:photo.image name:photo.imageName];
    
     NSLog(@"%@  imgPathsaveInfo",imgPath);
    
    
    
    [obj setValue:imgPath forKey:@"image_path"];
    [obj setValue:photo.imageDate forKey:@"date"];
    [obj setValue:photo.albumName forKey:@"album_name"];
    [obj setValue:photo.imageName forKey:@"image_name"];
    
    [self saveContext];
    
}



#pragma mark - Save/Load/Remove NSFileManager methods

-(NSString *)pathOfSavingImgToDocuments:(NSData *)imageData name:(NSString*)imageName{
   
    
   
      //  NSData *imageData = UIImageJPEGRepresentation(imageSaving,0.8); //convert image into .png format.
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
    
    
    
        NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
   // dispatch_async(dispatch_get_main_queue(), ^{

        [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
        NSLog(@"image saved");
  //  });
    
    return fullPath;
        
    }
    
    //removing an image

//-(void)checkFiles{
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsPath = [paths objectAtIndex:0];
//    
//    NSFileManager *manager = [[NSFileManager alloc] init];
//    NSDirectoryEnumerator *fileEnumerator = [manager enumeratorAtPath:documentsPath];
//    
//    for (NSString *filename in fileEnumerator) {
//        
//    }
//    
//   
//}

    - (void)removeImage:(NSString*)fileName {
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", fileName]];
        
        [fileManager removeItemAtPath: fullPath error:NULL];
        
        NSLog(@"image removed");
        
    }
    
    //loading an image
    
- (UIImage *)loadImage:(NSString*)imageName {
      
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        NSString *documentsDirectory = [paths objectAtIndex:0];
      
        NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
        UIImage *img =[UIImage imageWithContentsOfFile:fullPath];
        
        return img;
       
    }
#pragma mark - delete Photo


-(void)deleteInfo:(AlbumModel *)photo {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSEntityDescription *entity  = [NSEntityDescription entityForName:@"Photos" inManagedObjectContext:context];
    NSFetchRequest *request =[[NSFetchRequest alloc ]init];
    [request setEntity:entity];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"(image_path == %@)and(date == %@)",photo.imagePath, photo.imageDate];
    [request setPredicate:predicate];
    
    NSArray *objects = [context executeFetchRequest:request error:nil ];
    
    for (NSManagedObject *obj in objects) {
        [context deleteObject:obj];
        
    }
    [self removeImage:photo.imageName];
    
    [self saveContext];
    
  
    
}

- (void) deleteAllObjects: (NSString *) entityDescription  {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
   // [fetchRequest release];
    
    
    for (NSManagedObject *managedObject in items) {
        [self.managedObjectContext deleteObject:managedObject];
        NSLog(@"%@ object deleted",entityDescription);
    }
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
    
}


#pragma mark - Initialization

-(id)init {
    self =[super init];
    if (self) {
        
        [self initPhotosArray];
        }
    
    return self;
}





-(void)initPhotosArray {
    
    
    NSFetchRequest *request =[[NSFetchRequest alloc]initWithEntityName:@"Photos"];
    photos =[[NSMutableArray alloc]init];
    NSArray *tmp =[self.managedObjectContext executeFetchRequest:request error:nil];
    
    for (NSManagedObject *object in tmp) {
        
        
        AlbumModel *photo =[[AlbumModel alloc]init:nil defaultDate:[object valueForKey:@"date"] albumName:[object valueForKey:@"album_name"] image_path:[object valueForKey:@"image_path"] img_name:[object valueForKey:@"image_name"]];
        
        [photos addObject:photo];
  
        
        
        
}
   
   

}
- (NSArray *)photos
{
    return photos;
}

-(NSArray *) photoFromAlbum: (NSString *)album{
    NSMutableArray *array = [NSMutableArray array];
    long z = photos.count;
    for (int i = 0; i<z ; i++) {
         AlbumModel *photo = photos[i];
        if ([photo.albumName isEqualToString:album]) {
            
            [array addObject:photo];
        }

    }
        return array;
    
}

-(NSArray *)sortedPhotos{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"defaultDate"
                                                 ascending:NO];
    NSMutableArray *sortDescriptors = [NSMutableArray arrayWithObject:sortDescriptor];
    sortedPhotos = [photos sortedArrayUsingDescriptors:sortDescriptors];
    return sortedPhotos;
}






-(void)addPhoto:(AlbumModel *)photo  {
  //  [photos addObject:photo];
   [self saveInfo:photo];
    NSLog(@"%d sortedNotes index ", (int)[sortedPhotos indexOfObject:photo]);
    
 [self initPhotosArray];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"photosArrayWasChanged" object:nil userInfo:nil];
    
}
-(void)deletePhoto:(AlbumModel *)photo {
    [self deleteInfo:photo];
    [self initPhotosArray];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"photosArrayWasChanged" object:nil userInfo:nil];
  
}


-(void)setCurrentPhotoIndex: (int)currentIndex{
    currentIndexOfphoto = currentIndex;
    
}

-(int)getCurrentPhotoIndex{
    
    return currentIndexOfphoto;
    
}





@end
