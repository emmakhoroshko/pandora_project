//
//  SignIn_VC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 10.08.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "SignIn_VC.h"
#import "ViewController.h"

#import "SignUp_VC.h"
#import "BiometricAuthenticationFacade.h"


/*
 
 при регистрации узнать пароль узнать телефон
 
 1)Щшибка когда не вс поля заполнены в классе one password
 Прикинуть новый дизайн
 привязать кнопку отпечатка +
 Создать VC настроек
 Разобратья с обводкой фотографий при листании(
 ! didReceiveMemoryWarning! сделать везде убийство ненужных методов которые загружают память
 
 */
#define viewRadius 18;


@interface SignIn_VC (){
    
}

@end

@implementation SignIn_VC

#pragma - mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setDesignAllViews];
    [self makeNavigationBarHidden];
    [self setKeyboardNotifications];
    //  [self autentificationMethods];
    
    //  [self ableToUseButtons];
    
}


-(void)viewWillAppear:(BOOL)animated{
    //  [self ableToUseButtons];
    _passwordfField.text = nil;
    [super viewWillAppear:true];
}


-(void)ableToUseButtons{
    if (!(_managerSignUp.passwordInfo.idPassword==nil)) {
        _buttonCreateAccount.enabled =false;
        _buttonCreateAccount.titleLabel.textColor =[UIColor whiteColor];
        //        _buttonForgotPassword.enabled = true;
        //        _buttonForgotPassword.titleLabel.textColor =[UIColor blueColor];
        
    }else{
        //        _buttonCreateAccount.enabled =true;
        //        _buttonCreateAccount.titleLabel.textColor =[UIColor blueColor];
        
        _buttonForgotPassword.enabled =FALSE;
        _buttonForgotPassword.titleLabel.textColor =[UIColor whiteColor];
    }
    _passwordfField.text = nil;
}





#pragma - mark - Keyboard methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma - mark - Life cycle
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self removeKeyboardNotifications];
    
}

#pragma - mark - Login access


-(IBAction)logIn:(id)sender{
    
    [self performSegueWithIdentifier:@"ViewController" sender:nil];//временно
    //    if (([_manager.passwordInfo.idPassword isEqualToString: _passwordfField.text])) {
    //
    //
    //        [self performSegueWithIdentifier:@"ViewController" sender:nil];
    //
    //
    //
    //    }else{
    //        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Неверный пароль" message:@"Введен неверный пароль. Попробуйте еще раз." preferredStyle: UIAlertControllerStyleAlert ];
    //
    //        UIAlertAction * cancelAdditingAlbum = [UIAlertAction actionWithTitle:NSLocalizedString(@"try_one_more_time", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {} ];
    //
    //
    //        [alert addAction:cancelAdditingAlbum];
    //        [self presentViewController:alert animated:YES completion:nil];
    //
    //    };
    //
    
    
}







-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"ViewController" ]){
        
        
    }
    
    
}

#pragma - mark - Other methods


-(void)setDesignAllViews {
    [self resetView];
    [self passwordViewsDesign:_password_ViewTextField];
    [self designViewAsButton:_logIn_View];
}

- (IBAction)touchIdActivated:(id)sender {
    
    [self autentificationMethods];
}


-(void)autentificationMethods{
    if (!(_managerSignUp.passwordInfo.idPassword ==nil)) {
        
        BiometricAuthenticationFacade *bio =[[BiometricAuthenticationFacade alloc]init];
        if ([bio isPassByBiometricsAvailable]) {
            
            [bio passByBiometricsWithReason:@"Используйте отпечаток пальца для входа" succesBlock:^{
                //            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Подтверждено" message:@"Введен верный пароль" preferredStyle: UIAlertControllerStyleAlert ];
                //
                //            UIAlertAction * cancelAdditingAlbum = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {} ];
                //
                //
                //            [alert addAction:cancelAdditingAlbum];
                //            [self presentViewController:alert animated:YES completion:nil];
                
                [self performSegueWithIdentifier:@"ViewController" sender:nil];
                
            } failureBlock:^(NSError *error) {
                
                //            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Повторить" message:@"Используйте отпечаток пальца для входа" preferredStyle: UIAlertControllerStyleAlert ];
                //
                //            UIAlertAction * cancelAdditingAlbum = [UIAlertAction actionWithTitle:NSLocalizedString(@"try_one_more_time", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {} ];
                //
                //
                //            [alert addAction:cancelAdditingAlbum];
                //            [self presentViewController:alert animated:YES completion:nil];
            }];
        }
    }else{
        //  [self alertToCreateAccount];
    }
    
}
-(void)alertToCreateAccount{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Аккаунт не создан" message:@"Пожалуйста, создайте аккаунт для использования приложения." preferredStyle: UIAlertControllerStyleAlert ];
    
    UIAlertAction * cancelAdditingAlbum = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {} ];
    
    
    [alert addAction:cancelAdditingAlbum];
    [self presentViewController:alert animated:YES completion:nil];
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
