//
//  NotesModel.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 13.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotesModel : NSObject

@property (strong, nonatomic) NSString * noteTitle;
@property (strong, nonatomic) NSString * noteBody;
@property (strong, nonatomic) NSString * noteDate;
@property (strong, nonatomic) NSDate * defaultDate;
@property (strong, nonatomic) NSString * pwTitle;
@property (strong, nonatomic) NSString * pwLogin;
@property (strong, nonatomic) NSString * pwPassword;
@property (strong, nonatomic) NSString * pwAdditionalInfo;
@property (strong, nonatomic) NSString * pwComment;
@property (strong, nonatomic) NSDate * pwDate;

@property (strong, nonatomic) NSString * idPassword;
@property (strong, nonatomic) NSString * idMail;


- (id)initWithTitle:(NSString *)title
           noteBody:(NSString *)noteBody
               date:(NSString *)noteDate
        defaultDate:(NSDate *)defaultDate;


-(id)initPasswordWithTitle:(NSString *)title
                     login:(NSString *)login
                  password:(NSString *)password
            additionalInfo:(NSString *)additionalInfo
                   comment:(NSString *)comment
                      date:(NSDate *)date;

-(id)initAccesPassword:(NSString *)passwordSignUp
                  mail:(NSString *) mailSignUp;

@end
