//
//  Transition_VC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 31.07.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Transition_VC : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic) BOOL presenting ;
@property (nonatomic) CGRect originFrame ;
@property (nonatomic) UIView * container ;
@property (nonatomic) UIView * noteView1 ;
@property (nonatomic) UIView * passView1 ;
@property (nonatomic) NSArray * animatViews;
@property (nonatomic, strong) UIVisualEffectView *visualEffectView;
@property (nonatomic,strong) UIImageView * image;

@property (nonatomic) CGRect originNoteFrame ;
@end
