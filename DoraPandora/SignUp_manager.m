//
//  SignUp_manager.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 11.08.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "SignUp_manager.h"


@implementation SignUp_manager{
    NotesModel *passwordInfo;
}

-(id)init{
    self =[super init];
    if (self) {
        passwordInfo =[[NotesModel alloc]init];
    }
    return self;
    
}

-(NotesModel *)passwordInfo{
    return passwordInfo;
}

-(void)setPassword:(NotesModel *)password {
    passwordInfo = password;
}
 

@end
