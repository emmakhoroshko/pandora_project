//
//  ViewController.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 12.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "ViewController.h"
#import "NoteManager.h"
#import "ShowAllNotes_VC.h"
#import "ViewDesign.h"
#import "ShowAllPasswords_VC.h"
#import "ColorsManager.h"
#import "PasswordManager.h"
#import "ShowAllAlbums_VC.h"
#import "MediaManager.h"
#import "PhotoManager.h"
#import "Transition_VC.h"
#import "Searching Screen_VC.h"
#import "GeneralSettingsVC.h"

#define duration 0.3


@interface ViewController ()<UIViewControllerTransitioningDelegate,UIGestureRecognizerDelegate> {
    
    UIStoryboard *storyboard;
    NoteManager *manager;
    PasswordManager *pwManager;
    MediaManager *mediaManager;
    PhotoManager *photoManager;
    Transition_VC *transition;
    UIView *selectedView;
    NSArray *imageViewsArray;
    UINavigationController *NAVIGATION;
    
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    imageViewsArray = [NSArray arrayWithObjects:_viewNote,_viewPassword,_viewMedia,nil];
    
    [ViewDesign setRoundView:_viewNote andImage: _notesIcon];
    [ViewDesign setRoundView:_viewMedia andImage: _mediaIcon];
    [ViewDesign setRoundView:_viewPassword andImage:_passwordIcon];
    
    
    manager = [[NoteManager alloc]init];
    pwManager =[[PasswordManager alloc]init];
    mediaManager =[[MediaManager alloc]init];
    photoManager = [[PhotoManager alloc]init];
    
    self.transitioningDelegate = self;
    
    
    transition = [[Transition_VC alloc]init]; //
    transition.animatViews = imageViewsArray; // передаю массив трех views в класс для обработки перехода ( 1) вариант для возвращения вьюшек после завершения transition)
    [self setIcons];
    [self makeNavigationBarHidden];
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self gestureRecognizer]; //
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    
    //    for (UIView *view1 in imageViewsArray.reverseObjectEnumerator) {
    //
    //    [self.view bringSubviewToFront:view1]; ( 2 вариант для возвращения вьюшек после завершения transition)
    //    }
    //
    //  transition.animatViews = imageViewsArray;
    
    
}
#pragma mark - gesture Recognizer

-(UITapGestureRecognizer*)setRecognizer{
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(activateTransition:)];
    [gestureRecognizer setNumberOfTapsRequired:1];
    return gestureRecognizer;
}


-(void)gestureRecognizer{
    
    
    [_viewNote addGestureRecognizer:[self setRecognizer]];
    [_viewNote setUserInteractionEnabled: true];
    [_viewPassword addGestureRecognizer:[self setRecognizer]];
    [_viewPassword setUserInteractionEnabled: true];
    [_viewMedia addGestureRecognizer:[self setRecognizer]];
    [_viewMedia setUserInteractionEnabled: true];
    
    
}


#pragma mark - Transition action

-(IBAction)activateTransition:(UITapGestureRecognizer *)tap{
    
    selectedView = tap.view;
    //  [selectedView setHidden: true];
    int view = (int)[imageViewsArray indexOfObject:selectedView];
    NSLog(@"%f _viewNote.bounds -2",_viewNote.frame.origin.x);
    switch (view) {
        case (0) :
        {
            //  ShowAllNotes_VC *noteVC = [[ShowAllNotes_VC alloc]initWithNibName:@"ShowAllNotes_VC" bundle:nil];
            ShowAllNotes_VC *noteVC  = [storyboard instantiateViewControllerWithIdentifier:@"ShowAllNotes_VC"];
            
            noteVC.manager = manager;
            noteVC.view.layer.cornerRadius = 20;
            NAVIGATION = [[UINavigationController alloc] initWithRootViewController:noteVC];
            
            // noteVC.labelStartedPoint = _notesButton.center;
            
            noteVC.animateTransition = true;
            noteVC.presenting =true;
            //            transition.noteView1 =nil;
            //            transition.passView1 = nil; ( 3 вариант для возвращения вьюшек после завершения transition)
            
            //   transition.animatViews = nil;
            
        }
            break;
        case (1) :{
            ShowAllPasswords_VC *passwVC =[[ShowAllPasswords_VC alloc]initWithNibName:@"ShowAllPasswords_VC" bundle:nil];
            passwVC =[storyboard instantiateViewControllerWithIdentifier:@"ShowAllPasswords_VC"];
            passwVC.pwManager = pwManager;
            passwVC.view.layer.cornerRadius =20;
            NAVIGATION = [[UINavigationController alloc] initWithRootViewController:passwVC];
            NSLog(@"%f _viewNote.bounds -1",_viewNote.frame.origin.x);
            
            passwVC.presenting =true;
            passwVC.animateTransition =true;
            //            transition.noteView1 =_viewNote;
            //            transition.passView1 = nil;
            
            //  transition.animatViews = @[_viewNote];
            // [_viewNote setHidden:true];
            
        }
            break;
        case (2) :{
            ShowAllAlbums_VC *albumsVC =[[ShowAllAlbums_VC alloc]initWithNibName:@"ShowAllAlbums_VC" bundle:nil];
            albumsVC =[storyboard instantiateViewControllerWithIdentifier:@"ShowAllAlbums_VC"];
            albumsVC.manager = mediaManager;
            albumsVC.photoManager =photoManager;
            albumsVC.view.layer.cornerRadius = 20;
            NAVIGATION = [[UINavigationController alloc] initWithRootViewController:albumsVC];
            albumsVC.presenting =true;
            albumsVC.animateTransition = true;
            //            transition.noteView1 =_viewNote;
            //            transition.passView1 = _viewPassword;
            //  transition.animatViews = @[_viewPassword,_viewNote];
            
            
            
        }
            
            break;
        default:
            break;
    }
    
    
    NAVIGATION.transitioningDelegate = self;
    [NAVIGATION setModalPresentationStyle: UIModalPresentationFullScreen];
    [self presentViewController: NAVIGATION animated:YES completion:nil];
    
    // [self animateMovingRightViews:selectedView];
}

-(void)animateMovingRightViews: (UIView *) tappedView { // описано движение вправо views
    NSArray *viewsArray = [NSArray arrayWithObjects:_viewNote,nil];
    
    [UIView animateWithDuration:duration delay:0.0 options:0.0 animations:^{
        
        for (UIView *view in viewsArray.reverseObjectEnumerator){
            
            view.center =(transition.presenting)? CGPointMake(view.center.x+500, view.center.y):CGPointMake(view.center.x-500, view.center.y);
            
        }
        
        
    } completion:^(BOOL finished) {
        NSLog(@"%f _viewNote.bounds -6",_viewNote.frame.origin.x);
    }];
    
    
}




-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    
    transition.originFrame = [selectedView.superview convertRect:selectedView.frame toView:nil];
    transition.presenting = true;
    //[self animateMovingRightViews:selectedView];
    NSLog(@"%f _viewNote.bounds 1",_viewNote.frame.origin.x);
    return transition;
    
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    transition.presenting = false;
    // [self animateMovingRightViews:selectedView];
    NSLog(@"%f _viewNote.bounds 2",_viewNote.frame.origin.x);
    
    return transition;
    
}



//-------------------------


#pragma mark - Presenter DM

//- (void) CPCPresentViewController:(id)vc presentationStyle:(UIModalPresentationStyle)style completion:(void (^)(BOOL finished))completion {
//
//    ShowAllNotes_VC *noteVC = [[ShowAllNotes_VC alloc]initWithNibName:@"ShowAllNotes_VC" bundle:nil];
//                noteVC = [storyboard instantiateViewControllerWithIdentifier:@"ShowAllNotes_VC"];
//
//
//
//
//
//
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
//    noteVC.modalPresentationStyle = UIModalPresentationCustom;//UIModalPresentationCustom;
//    noteVC.transitioningDelegate = self;
//    nav.navigationBar.barStyle = UIBarStyleDefault;
//    self.transitioningDelegate = self;
//     [self presentViewController:noteVC animated:YES completion:nil];
// [self presentViewController:nav animated:YES completion:^{
//        if(completion)
//            completion(YES);
//    }];

//   _currentViewController = vc;

//}

#pragma mark - transitioningDelegate DM

//-(UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
//    CheckoutPresentationController *cpc = [[CheckoutPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
//    cpc.frameHeight = 165;
//    cpc.customDelegate = self;
//    return cpc;
//}






#pragma - mark - Receive Memory Warning


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (IBAction)openNotes:(id)sender {
//
//    [self performSegueWithIdentifier:@"SegueNotesData" sender:nil];
//
//}
//- (IBAction)openPasswords:(id)sender {
//    [self performSegueWithIdentifier:@"SeguePasswordData" sender:nil];
//
//}

//- (IBAction)openMedia:(id)sender {
//    [self performSegueWithIdentifier:@"ShowAllAlbums_VC" sender:nil];
//}

-(void)setIcons {
    _passwordIcon.image =[UIImage imageNamed:@"password_icon"];
    _notesIcon.image = [UIImage imageNamed:@"note_icon"];
    _mediaIcon.image = [UIImage imageNamed:@"media_icon"];
}

#pragma mark - Search and Settings buttons



- (IBAction)searchItemsAction:(id)sender {
    
    Searching_Screen_VC *vc =[[Searching_Screen_VC alloc]initWithNibName:@"Searching_Screen_VC" bundle:nil];
    vc =[storyboard instantiateViewControllerWithIdentifier:@"Searching_Screen_VC"];
    //  selectedView = _searchItems.maskView;
    vc.pwManager = pwManager;
    vc.noteManager = manager;
    vc.mediaManager = mediaManager;
    // Configure the view controller.
    
    // Display the view controller
    NAVIGATION = [[UINavigationController alloc] initWithRootViewController:vc];
    //    NAVIGATION.transitioningDelegate = self;
    [NAVIGATION setModalPresentationStyle: UIModalPresentationPopover];
    [self.navigationController presentViewController: NAVIGATION animated:YES completion:nil];
    
    
    
}

- (IBAction)openSettings:(id)sender {
    
    GeneralSettingsVC *vc =[[GeneralSettingsVC alloc]initWithNibName:@"GeneralSettingsVC" bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"GeneralSettingsVC"];
    NAVIGATION =[[UINavigationController alloc]initWithRootViewController:vc];
    [NAVIGATION setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [self.navigationController presentViewController: NAVIGATION animated:YES completion:nil];
    
    
}


//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if ([segue.identifier isEqualToString:@"Searching_Screen_VC"]){
//
//            Searching_Screen_VC *vc = [segue destinationViewController];
//            vc.pwManager = pwManager;
//        vc.noteManager = manager;
//        vc.mediaManager = mediaManager;
//        [self presentViewController: vc animated:YES completion:nil];
//
//    }
//}

@end
