//
//  MediaManager.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 17.06.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AlbumModel.h"
#import <CoreData/CoreData.h>

@interface MediaManager : NSObject{
    NSMutableArray * albums;   
    NSArray * sortedAlbums;
    
}



-(NSArray *)sortedAlbums;

-(void)addAlbum:(AlbumModel *)album;
-(void)deleteAlbumAtIndex:(AlbumModel *)album;
-(NSArray *)albums;
- (void)setAlbumCover:(UIImage *)image forAlbumName:(NSString *)name;
@end
