//
//  NoteManager.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 13.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "NoteManager.h"

@interface NoteManager ()
{
    NSMutableArray * notes;
    NSArray * sortedNotes;
    // Массив всех notes
}
@end

@implementation NoteManager


- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}



- (void)saveInfo:(NotesModel*)newNote {
    
   
   NSManagedObject * notesObj = [NSEntityDescription insertNewObjectForEntityForName:@"Notes" inManagedObjectContext:self.managedObjectContext];
    
        
        [notesObj setValue:newNote.noteTitle forKey:@"title"];
        [notesObj setValue:newNote.noteBody forKey:@"body"];
        [notesObj setValue:newNote.defaultDate forKey:@"date"];
     [self saveContext];
     [self initNotes];
}


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            
            NSLog(@"Saving didn't work so well.. Error: %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void)deleteInfo:(NotesModel *)note {
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Notes" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
        [request setEntity:entityDescription];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"(date==%@)and(body==%@)", note.defaultDate,note.noteBody];
    [request setPredicate:predicate];
  
    NSArray *objects =[context executeFetchRequest:request error:nil];
    for (NSManagedObject *obj in objects) {
         [context deleteObject:obj];
    }
    
   
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    [self initNotes];
    
}

-(void)initNotes{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]
                                    initWithEntityName:@"Notes"];
    
    notes =[[NSMutableArray alloc]init];
    //    осуществляем запрос
    NSArray *tmpData = [self.managedObjectContext executeFetchRequest:fetchRequest
                                                                error:nil];
    
    //    обрабатываем запрос
    for (NSManagedObject *object in tmpData){
        
        if([object valueForKey:@"date"] == nil)
            continue;
        
        NotesModel *note =[[NotesModel alloc]initWithTitle:[object valueForKey:@"title"] noteBody: [object valueForKey:@"body"] date:[self dateFormatted:[object valueForKey:@"date"]] defaultDate:[object valueForKey:@"date"]];
        
        
        [notes addObject:note];
    }
    [self saveContext];
    

}

- (id)init
{
    self = [super init];
    if (self)
    {
        
        [self initNotes];
        //    конфигурируем запрос на получение друзей
           }
        
       return self;
}

- (NSArray *)notes
{
    return notes;
}

-(NSArray *)sortedNotes{
    NSSortDescriptor *sortDescriptor;
     sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"defaultDate"
                                                   ascending:NO];
       NSMutableArray *sortDescriptors = [NSMutableArray arrayWithObject:sortDescriptor];
        sortedNotes = [notes sortedArrayUsingDescriptors:sortDescriptors];
       return sortedNotes;
}


-(void)addNote:(NotesModel *)note  {
    
    NSLog(@"%d sortedNotes index ", (int)[sortedNotes indexOfObject:note]);
    [self saveInfo:note];
    }
   

    
-(void)deleteNote:(NotesModel* )note {
    
    
        NSLog(@" deleteNoteAtIndex %ld",(long)index);
        //[notes removeObjectAtIndex:index];
        [self deleteInfo:note];
    
   }


-(NSString *)dateFormatted:(NSDate *)date  {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    
    NSString *s = [dateFormatter stringFromDate:date];
    return s;
}

-(SearchScreen_Cell_VC *)searchNoteContainString:(NSString*)string indexPath:(NSIndexPath *)index tableview: (UITableView *)tableView{
    SearchScreen_Cell_VC *cell =[tableView dequeueReusableCellWithIdentifier:@"SearchScreen_Cell_VC"];
   
    NSArray *objects = [self searchNotesContainString:string];
    if (objects.count >0) {
        
        
        NotesModel *obj =objects[index.item];
        NSLog(@"%ld (long)index.item",(long)index.item);
        
        cell.mainLabel.text =obj.noteTitle;
        cell.passwordIfExist.text=@"";
        cell.sectionNameLabel.text = @"Notes";
        cell.passwordNameLabel.text =@"";
        cell.dateLabel.text = obj.noteDate;
        
    }
    return cell;
    
}

-(NSArray *)searchNotesContainString:(NSString*)string{
    NSMutableArray *array =
    [NSMutableArray arrayWithArray:notes];
    
    //    for ( NotesModel *password in  array) {
    
    //    NSManagedObjectContext *context = [self managedObjectContext];
    //
    //    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Passwords" inManagedObjectContext:context];
    //    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    //    [request setEntity:entityDescription];
    
    // array now contains { @"Chris", @"Melissa" }or(title  CONTAINS[cd] '%@')or(additionalInfo  CONTAINS[cd] '%@')or(password  CONTAINS[cd] '%@')
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"(_noteTitle CONTAINS[cd] %@)or(_noteBody  CONTAINS[cd] %@)",string,string];
    
    [array filterUsingPredicate:predicate];
    
    
    return array;
    
}
/*_noteTitle = title;
 _noteBody = noteBody;
 _noteDate = noteDate;
 _defaultDate = defaultDate;
*/

@end
