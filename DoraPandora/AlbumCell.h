//
//  AlbumCell.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 16.06.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *album;
@property (strong, nonatomic) IBOutlet UILabel *albumName;
@property (strong, nonatomic) IBOutlet UIImageView *photo;
@property (strong, nonatomic) IBOutlet UIImageView *photosOneLine;
@property (strong, nonatomic) IBOutlet UIImageView *photoMacroView_OnePhotoVC;

@end
