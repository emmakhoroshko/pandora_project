//
//  SignUp_VC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 10.08.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "SignUp_VC.h"
#import "SignIn_VC.h"
#import "SignUp_manager.h"
#import "NotesModel.h"


@interface SignUp_VC () <UITextFieldDelegate>

@end

@implementation SignUp_VC{
    SignUp_manager *manager;
}

#pragma - mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setAllUIElements];
    [self setKeyboardNotifications];
    manager = [[SignUp_manager alloc]init];
    [self makeNavigationBarHidden];
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [self hideKeyboard];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    NotesModel *model =[manager passwordInfo];
    if (!(model.idPassword == nil)) {
        [self performSegueWithIdentifier:@"SignIn" sender:nil];
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self removeKeyboardNotifications];
    // Dispose of any resources that can be recreated.
}


#pragma - mark - LogUp methods

- (IBAction)logUp:(id)sender {
    //    [_passwordReplyTextFIeld resignFirstResponder];
    //    [_passwordTextField resignFirstResponder];
    //    [self checkCorrectPassword];
    //
    //
    
    [self performSegueWithIdentifier:@"SignIn" sender:nil];//временный вариант для быстроты
    
}







-(void)checkCorrectPassword {
    if ([_passwordTextField.text isEqualToString:@""]) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Поля не заполнены" message:@"Введите пароль." preferredStyle: UIAlertControllerStyleAlert ];
        
        UIAlertAction * cancelAdditingAlbum = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {} ];
        
        
        [alert addAction:cancelAdditingAlbum];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        if((_passwordTextField.text.length<5)) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Данные введены некорректно" message:@"Проверьте правильность введенного пароля. Длина корректного пароля 5 и более символов." preferredStyle: UIAlertControllerStyleAlert ];
            
            UIAlertAction * cancelAdditingAlbum = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {} ];
            
            [alert addAction:cancelAdditingAlbum];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            
            if (_passwordReplyTextField.text ==_passwordTextField.text) {
                
                
                NotesModel *password  =[[NotesModel alloc] initAccesPassword:_passwordTextField.text mail:_passwordReplyTextField.text];
                [manager setPassword:password];
                
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Данные сохранены" message:@"Ваш пароль успешно сохранен" preferredStyle: UIAlertControllerStyleAlert ];
                
                UIAlertAction * cancelAdditingAlbum = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    
                    [self performSegueWithIdentifier:@"SignIn" sender:nil];
                    
                } ];
                
                [alert addAction:cancelAdditingAlbum];
                [self presentViewController:alert animated:YES completion:nil];
                _passwordTextField.text =@"";
                _passwordReplyTextField.text =@"";
                
            }else{
                
                
                UIAlertController *alert =[UIAlertController alertControllerWithTitle:@"Пароли не совпадают" message:@"Проверьте правильность введенных паролей" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancel =[UIAlertAction actionWithTitle:@"Попробовать снова" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                [alert addAction:cancel];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
            
        }
        
    }
    
}
#pragma - mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"SignIn"]) {
//        SignIn_VC *vc = [segue destinationViewController];
//       // vc.manager = manager;
        
    }
    [segue destinationViewController];
    
}

#pragma - mark - Keyboard methods

-(void)hideKeyboard{
    
    [self.view endEditing:YES];
//
//    [self.passwordReplyTextField resignFirstResponder];
//    [self.passwordTextField resignFirstResponder];
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [ self hideKeyboard];
    
}

#pragma - mark - Other methods

-(void)setAllUIElements{
    [self resetView];
    [self createTransitButton:_buttonSignUp];
    [self passwordViewsDesign:_password_ViewTextField];
    [self passwordViewsDesign:_passwordReply_ViewTextField];
    
    _linePasReply.backgroundColor = [ColorsManager viewsBorderColor];
    _linePassword.backgroundColor = [ColorsManager viewsBorderColor];
    
}

- (IBAction)replyPasswordIsChanging:(id)sender {
    
    if (_passwordReplyTextField.text ==_passwordTextField.text) {
        
        _imgReplyPassword.image =[UIImage imageNamed:@"checkDone"];
        
        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            _imgReplyPassword.transform = CGAffineTransformScale(CGAffineTransformIdentity ,1.5,1.5);
            [self changeColorWhenCorrectPasswordWrote];
            
        } completion:^(BOOL finished) {
            _imgReplyPassword.transform = CGAffineTransformScale(CGAffineTransformIdentity ,1,1);;
            
        }];
        [ self hideKeyboard];
    }else{
        
        NSLog(@"%@",_passwordReplyTextField.text);
    }
}

-(void)changeColorWhenCorrectPasswordWrote{
    
    _password_ViewTextField.layer.borderColor =[[ColorsManager correctPasswordColor]CGColor];
    _passwordReply_ViewTextField.layer.borderColor =[[ColorsManager correctPasswordColor]CGColor];
    _linePasReply.backgroundColor = [ColorsManager correctPasswordColor];
    _linePassword.backgroundColor = [ColorsManager correctPasswordColor];
    
}


@end
