//
//  CommonPhotosVC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 04.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "CommonPhotosVC.h"

@interface CommonPhotosVC () <CTAssetsPickerControllerDelegate>

@end

@implementation CommonPhotosVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)callCommonDataPicker {
    CTAssetsPickerController *pickerC = [[CTAssetsPickerController alloc] init];
    pickerC.delegate = self;
    [self presentViewController:pickerC animated:YES completion:nil];
}




-(void)newItem:(NSData *)photoData withNamePrefix:(NSString*)namePrefix{
    
    
    NSDate *now =[NSDate date];
    NSDateFormatter *dateForName =[[NSDateFormatter alloc]init];
    
    dateForName.dateFormat=@"yyyyMMdd'T'HHmmss";
    NSString *date = [dateForName stringFromDate:now];
    
    AlbumModel *photoNew  = [[AlbumModel alloc]init:photoData defaultDate:now albumName:_albumName image_path:nil img_name:[NSString stringWithFormat:@"%@%@",namePrefix,date]];
    
    [_manager addPhoto:photoNew];
     //  [_photosCollectionView reloadData];
    //  photoNew = nil ;
}





-(void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    //[self resetPhotos];
    self.requestOptions = [[PHImageRequestOptions alloc] init];
    self.requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    self.requestOptions.deliveryMode = PHVideoRequestOptionsDeliveryModeHighQualityFormat;
    self.requestOptions.synchronous = true;
    assets = [NSMutableArray arrayWithArray:assets];
    PHImageManager *managerPH = [PHImageManager defaultManager];
    
    for (PHAsset *asset in assets) {
        
        [managerPH requestImageDataForAsset:asset options:self.requestOptions resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
            [self newItem:imageData withNamePrefix:asset.localIdentifier.pathComponents[0]];
        }];
        
        NSLog(@"asset.localIdentifier %@", asset.localIdentifier.pathComponents[0]);
        
        
    }
    [_manager initPhotosArray];
    [self dismissViewControllerAnimated:YES completion:nil];
    [ self alertToDeleteImagesFromGallery:assets];
    
    
    
}


-(void)alertToDeleteImagesFromGallery: (NSArray *) assets{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Удалить фотографии" message:@"Вы точно хотите удалить фото из галереи?" preferredStyle: UIAlertControllerStyleAlert ];
    
    UIAlertAction * cancelDeleting= [UIAlertAction actionWithTitle:NSLocalizedString(@"нет", nil) style:UIAlertActionStyleDefault  handler:^(UIAlertAction * _Nonnull action) {} ];
    
    UIAlertAction * agreeToDelete = [UIAlertAction actionWithTitle:NSLocalizedString(@"Да", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            [PHAssetChangeRequest deleteAssets:assets];
        } completionHandler:^(BOOL success, NSError *error) {
            NSLog(@"Finished deleting asset. %@", (success ? @"Success." : error));
        }];
        
        
        //  [PHAssetChangeRequest deleteAssets:assets];
    }];
    [alert addAction:cancelDeleting];
    [alert addAction:agreeToDelete];
    [self presentViewController:alert animated:YES completion:nil];
    
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
