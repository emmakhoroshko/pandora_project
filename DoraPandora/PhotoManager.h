//
//  PhotoManager.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 20.06.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AlbumModel.h"
#import <CoreData/CoreData.h>

@interface PhotoManager : NSObject

-(void)setCurrentPhotoIndex: (int)currentIndex;
-(int)getCurrentPhotoIndex;


-(void)initPhotosArray;
-(NSArray *)sortedPhotos;
-(void)addPhoto:(AlbumModel *)photo;
-(void)deletePhoto:(AlbumModel *)photo;

- (NSArray *)photos;
-(NSArray *) photoFromAlbum: (NSString *)album;
//-(void)checkFiles;
- (UIImage*)loadImage:(NSString*)imageName;

@end
