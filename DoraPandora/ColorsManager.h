//
//  ColorsManager.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 21.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorsManager : UIColor


+(UIColor *)colorWithR:(CGFloat)red g:(CGFloat)green b:(CGFloat)blue alpha:(CGFloat)alpha;
+(UIColor *)textFieldNoteColor;
+(UIColor *)iconsTintNoteColor;
+(UIColor *)mainNoteColor;
+(UIColor *)mainPasswordColor;
+(UIColor *)mainMediaColor;
+(UIColor *)buttonsColor;

+(UIColor *)navigationBarIconsColor;
+(UIColor *)iconsTintPasswordColor;
+(UIColor *)viewsBorderColor;
+(UIColor *)correctPasswordColor;

@end
