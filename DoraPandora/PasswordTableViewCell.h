//
//  PasswordTableViewCell.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 22.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PasswordTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *passwordTitle;
@property (weak, nonatomic) IBOutlet UILabel *passwordLogin;
@property (weak, nonatomic) IBOutlet UILabel *pwAdditionalInfo;
@property (weak, nonatomic) IBOutlet UILabel *pwPassword;

@end
