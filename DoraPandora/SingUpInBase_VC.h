//
//  SingUpInBase_VC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 28.09.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorsManager.h"
#import "CommonViewController.h"

@interface SingUpInBase_VC :  CommonViewController

@property (strong, nonatomic) IBOutlet UIView *leftDesignView;
@property (strong, nonatomic) IBOutlet UIView *rightDesignView;
@property (strong, nonatomic) IBOutlet UIView *middleDisignView;
@property (strong, nonatomic) IBOutlet UIButton *transitButton;





-(void)designViewAsButton:(UIView *) view;
-(void)createTransitButton:(UIButton *) button;
-(void)resetView;
-(void)passwordViewsDesign:(UIView *)view;


@end
