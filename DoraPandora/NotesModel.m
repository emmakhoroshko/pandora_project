//
//  NotesModel.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 13.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "NotesModel.h"

@implementation  NotesModel

- (id)initWithTitle:(NSString *)title
           noteBody:(NSString *)noteBody
               date:(NSString *)noteDate
        defaultDate:(NSDate *)defaultDate{
    
    self = [super init];
    if (self) {
        _noteTitle = title;
        _noteBody = noteBody;
        _noteDate = noteDate;
        _defaultDate = defaultDate;
    }
    return self;
    
}

-(id)initPasswordWithTitle:(NSString *)title
                     login:(NSString *)login
                  password:(NSString *)password
            additionalInfo:(NSString *)additionalInfo
                   comment:(NSString *)comment
                     date:(NSDate *)date{
    self =[super init];
    if (self) {
        _pwTitle =title;
        _pwLogin = login;
        _pwPassword=password;
        _pwAdditionalInfo =additionalInfo;
        _pwComment =comment;
        _pwDate = date;
        
    }
    return self;
}


-(id)initAccesPassword:(NSString *)passwordSignUp
                  mail:(NSString *) mailSignUp
{
    
    self =[super init];
    if(self) {
        _idMail = mailSignUp;
        _idPassword =passwordSignUp;
    }
    return self;
    
}

@end
