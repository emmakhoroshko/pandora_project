//
//  OnePassword_VC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 25.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PasswordManager.h"

@interface OnePassword_VC :  UIViewController <UITextFieldDelegate, UIScrollViewDelegate>
{
   
    UITextField * activeField; // указывает на активный элемент ввода
}


@property (strong,nonatomic) PasswordManager *manager;
@property (strong,nonatomic) NotesModel *password;
@property (nonatomic) int currentIndex;
@property (nonatomic) BOOL newPasswordExample;
@property (weak, nonatomic) IBOutlet UITextField *pw_name;
@property (weak, nonatomic) IBOutlet UITextField *pw_login;
@property (weak, nonatomic) IBOutlet UITextField *pw_password;
@property (weak, nonatomic) IBOutlet UITextField *pw_addInfo;
@property (weak, nonatomic) IBOutlet UITextField *pw_comment;
@property (weak, nonatomic) IBOutlet UILabel *pw_firstLetter;
@property (weak, nonatomic) IBOutlet UIView *pw_roundView;

@property (weak, nonatomic) IBOutlet UILabel *label_login;

@property (weak, nonatomic) IBOutlet UILabel *label_pass;
@property (weak, nonatomic) IBOutlet UILabel *label_addInfo;
@property (weak, nonatomic) IBOutlet UILabel *label_comment;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;

- (IBAction)backgroundTap:(UIControl *)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomOffset;

@end
