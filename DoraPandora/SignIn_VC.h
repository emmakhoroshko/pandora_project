//
//  SignIn_VC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 10.08.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import "ColorsManager.h"
#import "SignUp_manager.h"
#import "SingUpInBase_VC.h"


@interface SignIn_VC : SingUpInBase_VC {
    
    
    
}


@property (strong, nonatomic) IBOutlet UIButton *biometricalButton;
@property (strong, nonatomic) IBOutlet SignUp_manager *managerSignUp;
@property (strong, nonatomic) IBOutlet UIView *password_ViewTextField;
@property (strong, nonatomic) IBOutlet UIImageView *app_icon;
@property (strong, nonatomic) IBOutlet UILabel *label_writePassword;
@property (strong, nonatomic) IBOutlet UITextField *passwordfField;
@property (strong, nonatomic) IBOutlet UIView *line;
@property (strong, nonatomic) IBOutlet UIButton *buttonLogIn;
@property (strong, nonatomic) IBOutlet UIButton *buttonForgotPassword;
@property (strong, nonatomic) IBOutlet UIButton *buttonCreateAccount;
@property (strong, nonatomic) IBOutlet UIView *logIn_View;

@end
