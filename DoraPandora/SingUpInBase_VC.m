//
//  SingUpInBase_VC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 28.09.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "SingUpInBase_VC.h"
#define textViewsRadius 8
#define buttonViewsRadius 8
#define designViewsRadius 18
#define borderWidthView 1
#define topLayoutGide -20
#define screenHeight self.view.bounds.size.height
#define screenWidth self.view.bounds.size.width

@interface SingUpInBase_VC ()

@end

@implementation SingUpInBase_VC


- (void)resetView{
    
    _leftDesignView = [[UIView alloc]initWithFrame:(CGRectMake(0, topLayoutGide, screenWidth/2.5, screenHeight/4.5))];
    _leftDesignView.backgroundColor = [ColorsManager mainMediaColor];
    
    _middleDisignView = [[UIView alloc]initWithFrame:(CGRectMake(screenWidth/3, topLayoutGide, screenWidth/2.5, screenHeight/3.5))];
    _middleDisignView.backgroundColor = [ColorsManager mainPasswordColor];
    
    _rightDesignView = [[UIView alloc]initWithFrame:(CGRectMake( 2 * screenWidth/3, topLayoutGide, screenWidth/2.5, screenHeight/2.5))];
    _rightDesignView.backgroundColor = [ColorsManager mainNoteColor];
    _rightDesignView.layer.cornerRadius = designViewsRadius;
    _middleDisignView.layer.cornerRadius = designViewsRadius;
    _leftDesignView.layer.cornerRadius = designViewsRadius;
    
    [self.view addSubview:_leftDesignView];
    [self.view addSubview:_middleDisignView];
    [self.view addSubview:_rightDesignView];
    
}



-(void)createTransitButton:(UIButton *)button {
    button.layer.cornerRadius = buttonViewsRadius;
    [button sizeToFit];
    button.backgroundColor = [ColorsManager buttonsColor];
    [button setTitle: @"Зарегистрироваться" forState:UIControlStateNormal];
}


-(void)designViewAsButton:(UIView *)view {
    view.layer.cornerRadius = buttonViewsRadius;
    view.backgroundColor = [ColorsManager buttonsColor];
    
}


-(void)passwordViewsDesign:(UIView *)view {
    view.layer.cornerRadius = textViewsRadius;
    [view sizeToFit];
    view.layer.borderWidth = borderWidthView;
    view.layer.borderColor = [[ColorsManager viewsBorderColor]CGColor];
}

@end
