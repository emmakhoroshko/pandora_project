//
//  MediaManager.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 17.06.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "MediaManager.h"


@implementation MediaManager


-(NSManagedObjectContext*)managedObjectContext{
    NSManagedObjectContext *context =nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context =[delegate managedObjectContext];
    }
    return context;
}


-(void)saveContext{
    NSError *error = nil;
    NSManagedObjectContext *context = self.managedObjectContext;
    if (context != nil) {
        if ([context hasChanges] && ![context save:&error]) {
            
            NSLog(@"Saving didn't work so well.. Error: %@, %@", error, [error userInfo]);
            abort();
        }
        
    }
}

   

-(id)init {
    self =[super init];
    if (self) {
        [self initMedia];
    }
    return self;
    

}

-(void)initMedia{
    NSFetchRequest *request =[[NSFetchRequest alloc]initWithEntityName:@"Albums"];
    
    albums =[[NSMutableArray alloc]init];
    NSArray *tmp =[self.managedObjectContext executeFetchRequest:request error:nil];
    
    for (NSManagedObject *object in tmp) {
        
        AlbumModel *album = [[AlbumModel alloc]initAlbumCover:[UIImage imageWithData:[object valueForKey:@"cover_img"]] albumDate:[object valueForKey:@"date"] aName:[object valueForKey: @"name"]];
        
        [albums addObject:album];
    }
}


- (NSArray *)albums
{
    return albums;
}

- (void)setAlbumCover:(UIImage *)image forAlbumName:(NSString *)name{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSEntityDescription *entity  = [NSEntityDescription entityForName:@"Albums" inManagedObjectContext:context];
    NSFetchRequest *request =[[NSFetchRequest alloc ]init];
    [request setEntity:entity];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"(name ==  %@)",name];
    [request setPredicate:predicate];
    
    NSArray *objects =[context executeFetchRequest:request error:nil ];
    for (NSManagedObject *obj in objects) {
    [obj setValue:UIImageJPEGRepresentation(image,0.4) forKey:@"cover_img"];
    [self saveContext];
        
        }
    [self initMedia];
}

-(NSArray *)sortedAlbums{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"albumDate"
                                                 ascending:NO];
    NSMutableArray *sortDescriptors = [NSMutableArray arrayWithObject:sortDescriptor];
    sortedAlbums = [albums sortedArrayUsingDescriptors:sortDescriptors];
    return sortedAlbums;
}


-(void)addAlbum:(AlbumModel *)album  {
    
    
    [self saveInfo:album];
    
    NSLog(@"%d sortedNotes index ", (int)[sortedAlbums indexOfObject:album]);
     [self initMedia];
}

-(void)saveInfo:(AlbumModel *) album{
    
    NSManagedObject *obj =[NSEntityDescription insertNewObjectForEntityForName:@"Albums" inManagedObjectContext:self.managedObjectContext];
    
    [obj  setValue: UIImageJPEGRepresentation(album.albumCover,0.6) forKey:@"cover_img"];
    [obj setValue: album.albumDate forKey:@"date"];
    [obj setValue: album.aName forKey:@"name"];
    
    [self saveContext];
    
}


-(void)deleteAlbumAtIndex:(AlbumModel *)album {
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSEntityDescription *entity  = [NSEntityDescription entityForName:@"Albums" inManagedObjectContext:context];
    NSFetchRequest *request =[[NSFetchRequest alloc ]init];
    [request setEntity:entity];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"(name == %@)and(date == %@)",album.aName, album.albumDate];
    [request setPredicate:predicate];
    
    NSArray *objects =[context executeFetchRequest:request error:nil ];
    
    for (NSManagedObject *obj in objects) {
        [context deleteObject:obj];
        
    }
    
    [self saveContext];
    
    [self initMedia];

    
}






@end
