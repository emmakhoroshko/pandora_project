//
//  KeyboardManager_VC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 03.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyboardManager_VC : UIViewController

-(void) setKeyboardNotifications;
- (void) removeKeyboardNotifications;

@end
