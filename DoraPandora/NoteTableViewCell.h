//
//  NoteTableViewCell.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 15.05.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelNoteCell;
@property (weak, nonatomic) IBOutlet UILabel *labelNoteDateCell;


@end
