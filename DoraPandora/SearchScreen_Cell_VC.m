//
//  SearchScreen_Cell_VC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 31.08.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "SearchScreen_Cell_VC.h"

@implementation SearchScreen_Cell_VC

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
