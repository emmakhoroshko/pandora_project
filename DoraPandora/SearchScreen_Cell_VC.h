//
//  SearchScreen_Cell_VC.h
//  DoraPandora
//
//  Created by Emma Khoroshko on 31.08.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchScreen_Cell_VC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *mainLabel;
@property (strong, nonatomic) IBOutlet UILabel *sectionNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordIfExist;
@end
