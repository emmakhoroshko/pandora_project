//
//  KeyboardManager_VC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 03.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "KeyboardManager_VC.h"

@interface KeyboardManager_VC ()

@end

@implementation KeyboardManager_VC

- (void) setKeyboardNotifications{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (void) removeKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


-(void)keyboardWillShow:(NSNotification *) notification {
    
    [UIView animateWithDuration:0.25 animations:^
     {
         NSValue *value = [notification.userInfo objectForKey: UIKeyboardFrameEndUserInfoKey];
         CGRect keyEndFrame = [value CGRectValue];
         // CGSize keySize =keyEndFrame.size;
         CGRect keyboardFrame = [self.view convertRect:keyEndFrame fromView:nil];
         
         CGRect newFrame = [self.view frame];
         newFrame.origin.y = - keyboardFrame.size.height+40; // tweak here to adjust the moving position
         [self.view setFrame:newFrame];
         
     }completion:^(BOOL finished){}
     ];
}


-(void)keyboardWillHide:(NSNotification *) notification {
    [UIView animateWithDuration:0.25 animations:^
     {
         
         CGRect newFrame = self.view.frame;
         newFrame.origin.y = 0.0f;// tweak here to adjust the moving position
         self.view.frame = newFrame;
         
     }completion:^(BOOL finished)
     {
         
     }];
    
}



@end
