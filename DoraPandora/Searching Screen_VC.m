//
//  Searching Screen_VC.m
//  DoraPandora
//
//  Created by Emma Khoroshko on 30.08.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

#import "Searching Screen_VC.h"
#import "OnePassword_VC.h"
#import "SingleNote_VC.h"

@interface Searching_Screen_VC () <UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    NSArray *sections;
    UIStoryboard *storyboard;

}
@end

@implementation Searching_Screen_VC

#pragma - mark - Life cycle


- (void)viewDidLoad {
    [super viewDidLoad];
    sections = @[@"Password",@"Note"];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];

     storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _searchingResultsTableView.delegate =self;
    _searchingResultsTableView.dataSource =self;
    _searchBar.delegate = self;
    [self createNavigationBar];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    
    [_searchingResultsTableView reloadData];
    }

#pragma - mark - Table View methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
    SearchScreen_Cell_VC *cell =[_searchingResultsTableView dequeueReusableCellWithIdentifier:@"SearchScreen_Cell_VC"];
    if ([_searchBar.text isEqualToString:@""]) {
       _searchingResultsTableView.hidden = true;
      
    }else{
        NSString *searchWord = _searchBar.text;
        _searchingResultsTableView.hidden = false;
        
      //  NSString *sectionTitle = [sections objectAtIndex:indexPath.section];
        
        switch (indexPath.section) {
                
            case 0:
                
                cell =[_pwManager searchPasswordsContainString:searchWord indexPath:indexPath tableview:tableView];
                break;
                
            case 1:
                cell =[_noteManager searchNoteContainString:searchWord indexPath:indexPath tableview:tableView];
                
                break;
           
            default:
                break;
        }
        
        
    
    }
        return cell;
    
}




-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *searchWord = _searchBar.text;
     NSArray *itemsArray;
    
   
    if (![_searchBar.text isEqualToString:@""]){
        
        switch (section) {
            case 0:
                 NSLog(@"%ld section2",section);
             itemsArray = [_pwManager searchPasswordsContainString:searchWord];
                
                break;
            case 1:
                NSLog(@"%ld section2",section);
             itemsArray = [_noteManager searchNotesContainString:searchWord];
                
                break;
            default:
                
                break;
        }
   
    }
   

    return itemsArray.count;
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
   // return sections.count;
    return  sections.count;
    
}

#pragma - mark - Row selected


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSString *searchWord = _searchBar.text;
//    NSArray *itemsArray;
    
    
    if (![_searchBar.text isEqualToString:@""]){
        
        switch (indexPath.section) {
            case 0:
                 [self performSegueWithIdentifier:@"ShowPassword" sender:indexPath];
                
                break;
            case 1:
//                NSLog(@"%ld section2",indexPath.section);
//                itemsArray = [_pwManager searchPasswordsContainString:searchWord];
                [self performSegueWithIdentifier:@"ShowNote" sender:indexPath];
                break;
            default:
                
                break;
        }
        
    }

    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSString *searchWord = _searchBar.text;
    NSArray *itemsArray;
    NSIndexPath *indexPath =(NSIndexPath*)sender;
    if ([segue.identifier isEqualToString:@"ShowPassword"]) {
        
        OnePassword_VC *vc = [segue destinationViewController];
        

       itemsArray = [_pwManager searchPasswordsContainString:searchWord];
        
         vc.password = itemsArray[indexPath.item];
        vc.manager =_pwManager;
    }else if ([segue.identifier isEqualToString:@"ShowNote"]){
       
        
        SingleNote_VC *vc =[segue destinationViewController];
        itemsArray = [_noteManager searchNotesContainString:searchWord];
        vc.note = itemsArray[indexPath.item];
        vc.manager =_noteManager;
        
        
    }
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    [_searchingResultsTableView reloadData];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
     [searchBar resignFirstResponder];
    [searchBar setText:@""];
   
   // [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


-(void)createNavigationBar{
    
    
    UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Menu-50"] style:UIBarButtonItemStyleDone target:self action:@selector(backButton:)];
    
  
    self.navigationItem.leftBarButtonItem = back;
    
    
}
- (IBAction)backButton:(UIButton *)sender{
    [_searchBar resignFirstResponder];
    [_searchBar setText:@""];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
